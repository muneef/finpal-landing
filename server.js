require("source-map-support").install();
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _regenerator = __webpack_require__(1);
  
  var _regenerator2 = _interopRequireDefault(_regenerator);
  
  var _asyncToGenerator2 = __webpack_require__(2);
  
  var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);
  
  __webpack_require__(3);
  
  var _path = __webpack_require__(4);
  
  var _path2 = _interopRequireDefault(_path);
  
  var _express = __webpack_require__(5);
  
  var _express2 = _interopRequireDefault(_express);
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _server = __webpack_require__(7);
  
  var _server2 = _interopRequireDefault(_server);
  
  var _bodyParser = __webpack_require__(8);
  
  var _bodyParser2 = _interopRequireDefault(_bodyParser);
  
  var _routes = __webpack_require__(9);
  
  var _routes2 = _interopRequireDefault(_routes);
  
  var _Html = __webpack_require__(134);
  
  var _Html2 = _interopRequireDefault(_Html);
  
  var _assets = __webpack_require__(135);
  
  var _assets2 = _interopRequireDefault(_assets);
  
  var _config = __webpack_require__(41);
  
  var _routes3 = __webpack_require__(136);
  
  var _routes4 = _interopRequireDefault(_routes3);
  
  var _auth = __webpack_require__(141);
  
  var _mongoose = __webpack_require__(138);
  
  var _mongoose2 = _interopRequireDefault(_mongoose);
  
  var _passport = __webpack_require__(142);
  
  var _passport2 = _interopRequireDefault(_passport);
  
  var _jsonwebtoken = __webpack_require__(143);
  
  var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);
  
  var _account = __webpack_require__(144);
  
  var _account2 = _interopRequireDefault(_account);
  
  var _passportLocal = __webpack_require__(146);
  
  var _passportJwt = __webpack_require__(147);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var server = global.server = (0, _express2.default)();
  
  // Authentication
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  //
  // Connect to Database
  // -----------------------------------------------------------------------------
  _mongoose2.default.connect(_config.mongo_db, function (error) {
    if (error) console.error(error);else console.log('mongo connected');
  });
  
  // Configure Passport
  server.use(_passport2.default.initialize());
  server.use(_passport2.default.session());
  
  // passport.use(new LocalStrategy(Account.authenticate()));
  // passport.serializeUser((user, done) => {
  //   done(null, user._id);
  // });
  
  // jwt
  
  var opts = {};
  opts.secretOrKey = 'secret';
  opts.issuer = "finpal.aurut.com";
  opts.audience = "muneef.in";
  
  _passport2.default.use(new _passportJwt.Strategy(opts, function (jwt_payload, done) {
    _account2.default.findOne({ id: jwt_payload.sub }, function (err, user) {
      if (err) {
        return done(err, false);
      }
      if (user) {
        done(null, user);
      } else {
        done(null, false);
        // or you could create a new account
      }
    });
  }));
  
  _passport2.default.deserializeUser(function (id, done) {
    _account2.default.findById(id, function (err, user) {
      done(err, user);
    });
  });
  
  server.use(_bodyParser2.default.urlencoded({
    extended: true
  }));
  
  server.use(_bodyParser2.default.json());
  server.use('/api/v2', _routes4.default);
  server.use('/auth', _auth.init);
  
  server.use('/api/session', __webpack_require__(150).default);
  
  //
  // Register Node.js middleware
  // -----------------------------------------------------------------------------
  // server.use(bodyParser);
  server.use(_express2.default.static(_path2.default.join(__dirname, 'public')));
  
  //
  // Register API middleware
  // -----------------------------------------------------------------------------
  // server.use('/api/content', require('./api/content').default);
  server.use('/api/message', __webpack_require__(151).default);
  //
  // Register server-side rendering middleware
  // -----------------------------------------------------------------------------
  server.get('*', function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(req, res, next) {
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;
              return _context2.delegateYield(_regenerator2.default.mark(function _callee() {
                var statusCode, data, css, context, html;
                return _regenerator2.default.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        statusCode = 200;
                        data = { title: '', description: '', css: '', body: '', entry: _assets2.default.main.js };
                        css = [];
                        context = {
                          insertCss: function insertCss(styles) {
                            return css.push(styles._getCss());
                          },
                          onSetTitle: function onSetTitle(value) {
                            return data.title = value;
                          },
                          onSetMeta: function onSetMeta(key, value) {
                            return data[key] = value;
                          },
                          onPageNotFound: function onPageNotFound() {
                            return statusCode = 404;
                          }
                        };
                        _context.next = 6;
                        return _routes2.default.dispatch({ path: req.path, query: req.query, context: context }, function (state, component) {
                          data.body = _server2.default.renderToString(component);
                          data.css = css.join('');
                        });
  
                      case 6:
                        html = _server2.default.renderToStaticMarkup(_react2.default.createElement(_Html2.default, data));
  
                        res.status(statusCode).send('<!doctype html>\n' + html);
  
                      case 8:
                      case 'end':
                        return _context.stop();
                    }
                  }
                }, _callee, undefined);
              })(), 't0', 2);
  
            case 2:
              _context2.next = 7;
              break;
  
            case 4:
              _context2.prev = 4;
              _context2.t1 = _context2['catch'](0);
  
              next(_context2.t1);
  
            case 7:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined, [[0, 4]]);
    })),
        _this = undefined;
    return function (_x, _x2, _x3) {
      return ref.apply(_this, arguments);
    };
  }());
  
  //
  // Launch the server
  // -----------------------------------------------------------------------------
  server.listen(_config.port, function () {
    /* eslint-disable no-console */
    console.log('The server is running at http://localhost:' + _config.port + '/');
  });

/***/ },
/* 1 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/regenerator");

/***/ },
/* 2 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/asyncToGenerator");

/***/ },
/* 3 */
/***/ function(module, exports) {

  module.exports = require("babel-polyfill");

/***/ },
/* 4 */
/***/ function(module, exports) {

  module.exports = require("path");

/***/ },
/* 5 */
/***/ function(module, exports) {

  module.exports = require("express");

/***/ },
/* 6 */
/***/ function(module, exports) {

  module.exports = require("react");

/***/ },
/* 7 */
/***/ function(module, exports) {

  module.exports = require("react-dom/server");

/***/ },
/* 8 */
/***/ function(module, exports) {

  module.exports = require("body-parser");

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _regenerator = __webpack_require__(1);
  
  var _regenerator2 = _interopRequireDefault(_regenerator);
  
  var _asyncToGenerator2 = __webpack_require__(2);
  
  var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _Router = __webpack_require__(10);
  
  var _Router2 = _interopRequireDefault(_Router);
  
  var _fetch = __webpack_require__(39);
  
  var _fetch2 = _interopRequireDefault(_fetch);
  
  var _App = __webpack_require__(42);
  
  var _App2 = _interopRequireDefault(_App);
  
  var _HomePage = __webpack_require__(81);
  
  var _HomePage2 = _interopRequireDefault(_HomePage);
  
  var _ContentPage = __webpack_require__(104);
  
  var _ContentPage2 = _interopRequireDefault(_ContentPage);
  
  var _ContactPage = __webpack_require__(107);
  
  var _ContactPage2 = _interopRequireDefault(_ContactPage);
  
  var _AboutPage = __webpack_require__(110);
  
  var _AboutPage2 = _interopRequireDefault(_AboutPage);
  
  var _LoginPage = __webpack_require__(113);
  
  var _LoginPage2 = _interopRequireDefault(_LoginPage);
  
  var _RegisterPage = __webpack_require__(125);
  
  var _RegisterPage2 = _interopRequireDefault(_RegisterPage);
  
  var _NotFoundPage = __webpack_require__(128);
  
  var _NotFoundPage2 = _interopRequireDefault(_NotFoundPage);
  
  var _ErrorPage = __webpack_require__(131);
  
  var _ErrorPage2 = _interopRequireDefault(_ErrorPage);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var router = new _Router2.default(function (on) {
    on('*', function () {
      var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(state, next) {
        var component;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return next();
  
              case 2:
                component = _context.sent;
                return _context.abrupt('return', component && _react2.default.createElement(
                  _App2.default,
                  { context: state.context },
                  component
                ));
  
              case 4:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined);
      })),
          _this = undefined;
      return function (_x, _x2) {
        return ref.apply(_this, arguments);
      };
    }());
  
    on('/', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt('return', _react2.default.createElement(_HomePage2.default, null));
  
            case 1:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    })));
    on('/about', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3() {
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt('return', _react2.default.createElement(_AboutPage2.default, null));
  
            case 1:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined);
    })));
    on('/contact', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4() {
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              return _context4.abrupt('return', _react2.default.createElement(_ContactPage2.default, null));
  
            case 1:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, undefined);
    })));
    on('/login', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5() {
      return _regenerator2.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              return _context5.abrupt('return', _react2.default.createElement(_LoginPage2.default, null));
  
            case 1:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, undefined);
    })));
    on('/register', (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6() {
      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              return _context6.abrupt('return', _react2.default.createElement(_RegisterPage2.default, null));
  
            case 1:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, undefined);
    })));
  
    on('error', function (state, error) {
      return state.statusCode === 404 ? _react2.default.createElement(
        _App2.default,
        { context: state.context, error: error },
        _react2.default.createElement(_NotFoundPage2.default, null)
      ) : _react2.default.createElement(
        _App2.default,
        { context: state.context, error: error },
        _react2.default.createElement(_ErrorPage2.default, null)
      );
    });
  });
  
  exports.default = router;

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _slicedToArray2 = __webpack_require__(11);
  
  var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);
  
  var _regenerator = __webpack_require__(16);
  
  var _regenerator2 = _interopRequireDefault(_regenerator);
  
  var _getIterator2 = __webpack_require__(14);
  
  var _getIterator3 = _interopRequireDefault(_getIterator2);
  
  var _asyncToGenerator2 = __webpack_require__(30);
  
  var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);
  
  var _create = __webpack_require__(23);
  
  var _create2 = _interopRequireDefault(_create);
  
  var _classCallCheck2 = __webpack_require__(31);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(32);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _Route = __webpack_require__(35);
  
  var _Route2 = _interopRequireDefault(_Route);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var emptyFunction = function emptyFunction() {}; /**
                                                    * React Routing | http://www.kriasoft.com/react-routing
                                                    * Copyright (c) Konstantin Tarkus <hello@tarkus.me> | The MIT License
                                                    */
  
  var Router = function () {
  
    /**
     * Creates a new instance of the `Router` class.
     */
  
    function Router(initialize) {
      (0, _classCallCheck3.default)(this, Router);
  
      this.routes = [];
      this.events = (0, _create2.default)(null);
  
      if (typeof initialize === 'function') {
        initialize(this.on.bind(this));
      }
    }
  
    /**
     * Adds a new route to the routing table or registers an event listener.
     *
     * @param {String} path A string in the Express format, an array of strings, or a regular expression.
     * @param {Function|Array} handlers Asynchronous route handler function(s).
     */
  
    (0, _createClass3.default)(Router, [{
      key: 'on',
      value: function on(path) {
        for (var _len = arguments.length, handlers = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          handlers[_key - 1] = arguments[_key];
        }
  
        if (path === 'error') {
          this.events[path] = handlers[0];
        } else {
          this.routes.push(new _Route2.default(path, handlers));
        }
      }
    }, {
      key: 'dispatch',
      value: function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(state, cb) {
          var routes, handlers, value, result, done, next;
          return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  if (typeof state === 'string' || state instanceof String) {
                    state = { path: state };
                  }
                  cb = cb || emptyFunction;
                  routes = this.routes;
                  handlers = _regenerator2.default.mark(function _callee() {
                    var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, route, match, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, handler;
  
                    return _regenerator2.default.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _iteratorNormalCompletion = true;
                            _didIteratorError = false;
                            _iteratorError = undefined;
                            _context.prev = 3;
                            _iterator = (0, _getIterator3.default)(routes);
  
                          case 5:
                            if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                              _context.next = 38;
                              break;
                            }
  
                            route = _step.value;
                            match = route.match(state.path);
  
                            if (!match) {
                              _context.next = 35;
                              break;
                            }
  
                            _iteratorNormalCompletion2 = true;
                            _didIteratorError2 = false;
                            _iteratorError2 = undefined;
                            _context.prev = 12;
                            _iterator2 = (0, _getIterator3.default)(match.route.handlers);
  
                          case 14:
                            if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                              _context.next = 21;
                              break;
                            }
  
                            handler = _step2.value;
                            _context.next = 18;
                            return [match, handler];
  
                          case 18:
                            _iteratorNormalCompletion2 = true;
                            _context.next = 14;
                            break;
  
                          case 21:
                            _context.next = 27;
                            break;
  
                          case 23:
                            _context.prev = 23;
                            _context.t0 = _context['catch'](12);
                            _didIteratorError2 = true;
                            _iteratorError2 = _context.t0;
  
                          case 27:
                            _context.prev = 27;
                            _context.prev = 28;
  
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                              _iterator2.return();
                            }
  
                          case 30:
                            _context.prev = 30;
  
                            if (!_didIteratorError2) {
                              _context.next = 33;
                              break;
                            }
  
                            throw _iteratorError2;
  
                          case 33:
                            return _context.finish(30);
  
                          case 34:
                            return _context.finish(27);
  
                          case 35:
                            _iteratorNormalCompletion = true;
                            _context.next = 5;
                            break;
  
                          case 38:
                            _context.next = 44;
                            break;
  
                          case 40:
                            _context.prev = 40;
                            _context.t1 = _context['catch'](3);
                            _didIteratorError = true;
                            _iteratorError = _context.t1;
  
                          case 44:
                            _context.prev = 44;
                            _context.prev = 45;
  
                            if (!_iteratorNormalCompletion && _iterator.return) {
                              _iterator.return();
                            }
  
                          case 47:
                            _context.prev = 47;
  
                            if (!_didIteratorError) {
                              _context.next = 50;
                              break;
                            }
  
                            throw _iteratorError;
  
                          case 50:
                            return _context.finish(47);
  
                          case 51:
                            return _context.finish(44);
  
                          case 52:
                          case 'end':
                            return _context.stop();
                        }
                      }
                    }, _callee, this, [[3, 40, 44, 52], [12, 23, 27, 35], [28,, 30, 34], [45,, 47, 51]]);
                  })();
                  value = undefined, result = undefined, done = false;
  
                  next = function () {
                    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
                      var _handlers$next;
  
                      var _value, _value2, _match, _handler;
  
                      return _regenerator2.default.wrap(function _callee2$(_context2) {
                        while (1) {
                          switch (_context2.prev = _context2.next) {
                            case 0:
                              if (!((_handlers$next = handlers.next(), value = _handlers$next.value, done = _handlers$next.done, _handlers$next) && !done)) {
                                _context2.next = 16;
                                break;
                              }
  
                              _value = value;
                              _value2 = (0, _slicedToArray3.default)(_value, 2);
                              _match = _value2[0];
                              _handler = _value2[1];
  
                              state.params = _match.params;
  
                              if (!(_handler.length > 1)) {
                                _context2.next = 12;
                                break;
                              }
  
                              _context2.next = 9;
                              return _handler(state, next);
  
                            case 9:
                              _context2.t0 = _context2.sent;
                              _context2.next = 15;
                              break;
  
                            case 12:
                              _context2.next = 14;
                              return _handler(state);
  
                            case 14:
                              _context2.t0 = _context2.sent;
  
                            case 15:
                              return _context2.abrupt('return', _context2.t0);
  
                            case 16:
                            case 'end':
                              return _context2.stop();
                          }
                        }
                      }, _callee2, this);
                    }));
                    return function next() {
                      return ref.apply(this, arguments);
                    };
                  }();
  
                case 6:
                  if (done) {
                    _context3.next = 16;
                    break;
                  }
  
                  _context3.next = 9;
                  return next();
  
                case 9:
                  result = _context3.sent;
  
                  if (!result) {
                    _context3.next = 14;
                    break;
                  }
  
                  state.statusCode = typeof state.statusCode === 'number' ? state.statusCode : 200;
                  cb(state, result);
                  return _context3.abrupt('return');
  
                case 14:
                  _context3.next = 6;
                  break;
  
                case 16:
                  if (!this.events.error) {
                    _context3.next = 32;
                    break;
                  }
  
                  _context3.prev = 17;
  
                  state.statusCode = 404;
                  _context3.next = 21;
                  return this.events.error(state, new Error('Cannot found a route matching \'' + state.path + '\'.'));
  
                case 21:
                  result = _context3.sent;
  
                  cb(state, result);
                  _context3.next = 32;
                  break;
  
                case 25:
                  _context3.prev = 25;
                  _context3.t0 = _context3['catch'](17);
  
                  state.statusCode = 500;
                  _context3.next = 30;
                  return this.events.error(state, _context3.t0);
  
                case 30:
                  result = _context3.sent;
  
                  cb(state, result);
  
                case 32:
                case 'end':
                  return _context3.stop();
              }
            }
          }, _callee3, this, [[17, 25]]);
        }));
        return function dispatch(_x, _x2) {
          return ref.apply(this, arguments);
        };
      }()
    }]);
    return Router;
  }();
  
  exports.default = Router;

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

  "use strict";
  
  exports.__esModule = true;
  
  var _isIterable2 = __webpack_require__(12);
  
  var _isIterable3 = _interopRequireDefault(_isIterable2);
  
  var _getIterator2 = __webpack_require__(14);
  
  var _getIterator3 = _interopRequireDefault(_getIterator2);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  exports.default = (function () {
    function sliceIterator(arr, i) {
      var _arr = [];
      var _n = true;
      var _d = false;
      var _e = undefined;
  
      try {
        for (var _i = (0, _getIterator3.default)(arr), _s; !(_n = (_s = _i.next()).done); _n = true) {
          _arr.push(_s.value);
  
          if (i && _arr.length === i) break;
        }
      } catch (err) {
        _d = true;
        _e = err;
      } finally {
        try {
          if (!_n && _i["return"]) _i["return"]();
        } finally {
          if (_d) throw _e;
        }
      }
  
      return _arr;
    }
  
    return function (arr, i) {
      if (Array.isArray(arr)) {
        return arr;
      } else if ((0, _isIterable3.default)(Object(arr))) {
        return sliceIterator(arr, i);
      } else {
        throw new TypeError("Invalid attempt to destructure non-iterable instance");
      }
    };
  })();

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(13), __esModule: true };

/***/ },
/* 13 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/is-iterable");

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(15), __esModule: true };

/***/ },
/* 15 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/get-iterator");

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

  // This method of obtaining a reference to the global object needs to be
  // kept identical to the way it is obtained in runtime.js
  var g =
    typeof global === "object" ? global :
    typeof window === "object" ? window :
    typeof self === "object" ? self : this;
  
  // Use `getOwnPropertyNames` because not all browsers support calling
  // `hasOwnProperty` on the global `self` object in a worker. See #183.
  var hadRuntime = g.regeneratorRuntime &&
    Object.getOwnPropertyNames(g).indexOf("regeneratorRuntime") >= 0;
  
  // Save the old regeneratorRuntime in case it needs to be restored later.
  var oldRuntime = hadRuntime && g.regeneratorRuntime;
  
  // Force reevalutation of runtime.js.
  g.regeneratorRuntime = undefined;
  
  module.exports = __webpack_require__(17);
  
  if (hadRuntime) {
    // Restore the original runtime.
    g.regeneratorRuntime = oldRuntime;
  } else {
    // Remove the global property added by runtime.js.
    try {
      delete g.regeneratorRuntime;
    } catch(e) {
      g.regeneratorRuntime = undefined;
    }
  }
  
  module.exports = { "default": module.exports, __esModule: true };


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

  /* WEBPACK VAR INJECTION */(function(module) {"use strict";
  
  var _promise = __webpack_require__(19);
  
  var _promise2 = _interopRequireDefault(_promise);
  
  var _setPrototypeOf = __webpack_require__(21);
  
  var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);
  
  var _create = __webpack_require__(23);
  
  var _create2 = _interopRequireDefault(_create);
  
  var _typeof2 = __webpack_require__(25);
  
  var _typeof3 = _interopRequireDefault(_typeof2);
  
  var _iterator = __webpack_require__(28);
  
  var _iterator2 = _interopRequireDefault(_iterator);
  
  var _symbol = __webpack_require__(26);
  
  var _symbol2 = _interopRequireDefault(_symbol);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * Copyright (c) 2014, Facebook, Inc.
   * All rights reserved.
   *
   * This source code is licensed under the BSD-style license found in the
   * https://raw.github.com/facebook/regenerator/master/LICENSE file. An
   * additional grant of patent rights can be found in the PATENTS file in
   * the same directory.
   */
  
  !(function (global) {
    "use strict";
  
    var hasOwn = Object.prototype.hasOwnProperty;
    var undefined; // More compressible than void 0.
    var iteratorSymbol = typeof _symbol2.default === "function" && _iterator2.default || "@@iterator";
  
    var inModule = ( false ? "undefined" : (0, _typeof3.default)(module)) === "object";
    var runtime = global.regeneratorRuntime;
    if (runtime) {
      if (inModule) {
        // If regeneratorRuntime is defined globally and we're in a module,
        // make the exports object identical to regeneratorRuntime.
        module.exports = runtime;
      }
      // Don't bother evaluating the rest of this file if the runtime was
      // already defined globally.
      return;
    }
  
    // Define the runtime globally (as expected by generated code) as either
    // module.exports (if we're in a module) or a new, empty object.
    runtime = global.regeneratorRuntime = inModule ? module.exports : {};
  
    function wrap(innerFn, outerFn, self, tryLocsList) {
      // If outerFn provided, then outerFn.prototype instanceof Generator.
      var generator = (0, _create2.default)((outerFn || Generator).prototype);
      var context = new Context(tryLocsList || []);
  
      // The ._invoke method unifies the implementations of the .next,
      // .throw, and .return methods.
      generator._invoke = makeInvokeMethod(innerFn, self, context);
  
      return generator;
    }
    runtime.wrap = wrap;
  
    // Try/catch helper to minimize deoptimizations. Returns a completion
    // record like context.tryEntries[i].completion. This interface could
    // have been (and was previously) designed to take a closure to be
    // invoked without arguments, but in all the cases we care about we
    // already have an existing method we want to call, so there's no need
    // to create a new function object. We can even get away with assuming
    // the method takes exactly one argument, since that happens to be true
    // in every case, so we don't have to touch the arguments object. The
    // only additional allocation required is the completion record, which
    // has a stable shape and so hopefully should be cheap to allocate.
    function tryCatch(fn, obj, arg) {
      try {
        return { type: "normal", arg: fn.call(obj, arg) };
      } catch (err) {
        return { type: "throw", arg: err };
      }
    }
  
    var GenStateSuspendedStart = "suspendedStart";
    var GenStateSuspendedYield = "suspendedYield";
    var GenStateExecuting = "executing";
    var GenStateCompleted = "completed";
  
    // Returning this object from the innerFn has the same effect as
    // breaking out of the dispatch switch statement.
    var ContinueSentinel = {};
  
    // Dummy constructor functions that we use as the .constructor and
    // .constructor.prototype properties for functions that return Generator
    // objects. For full spec compliance, you may wish to configure your
    // minifier not to mangle the names of these two functions.
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
  
    var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype;
    GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
    GeneratorFunctionPrototype.constructor = GeneratorFunction;
    GeneratorFunction.displayName = "GeneratorFunction";
  
    // Helper for defining the .next, .throw, and .return methods of the
    // Iterator interface in terms of a single ._invoke method.
    function defineIteratorMethods(prototype) {
      ["next", "throw", "return"].forEach(function (method) {
        prototype[method] = function (arg) {
          return this._invoke(method, arg);
        };
      });
    }
  
    runtime.isGeneratorFunction = function (genFun) {
      var ctor = typeof genFun === "function" && genFun.constructor;
      return ctor ? ctor === GeneratorFunction ||
      // For the native GeneratorFunction constructor, the best we can
      // do is to check its .name property.
      (ctor.displayName || ctor.name) === "GeneratorFunction" : false;
    };
  
    runtime.mark = function (genFun) {
      if (_setPrototypeOf2.default) {
        (0, _setPrototypeOf2.default)(genFun, GeneratorFunctionPrototype);
      } else {
        genFun.__proto__ = GeneratorFunctionPrototype;
      }
      genFun.prototype = (0, _create2.default)(Gp);
      return genFun;
    };
  
    // Within the body of any async function, `await x` is transformed to
    // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
    // `value instanceof AwaitArgument` to determine if the yielded value is
    // meant to be awaited. Some may consider the name of this method too
    // cutesy, but they are curmudgeons.
    runtime.awrap = function (arg) {
      return new AwaitArgument(arg);
    };
  
    function AwaitArgument(arg) {
      this.arg = arg;
    }
  
    function AsyncIterator(generator) {
      // This invoke function is written in a style that assumes some
      // calling function (or Promise) will handle exceptions.
      function invoke(method, arg) {
        var result = generator[method](arg);
        var value = result.value;
        return value instanceof AwaitArgument ? _promise2.default.resolve(value.arg).then(invokeNext, invokeThrow) : _promise2.default.resolve(value).then(function (unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration. If the Promise is rejected, however, the
          // result for this iteration will be rejected with the same
          // reason. Note that rejections of yielded Promises are not
          // thrown back into the generator function, as is the case
          // when an awaited Promise is rejected. This difference in
          // behavior between yield and await is important, because it
          // allows the consumer to decide what to do with the yielded
          // rejection (swallow it and continue, manually .throw it back
          // into the generator, abandon iteration, whatever). With
          // await, by contrast, there is no opportunity to examine the
          // rejection reason outside the generator function, so the
          // only option is to throw it from the await expression, and
          // let the generator function handle the exception.
          result.value = unwrapped;
          return result;
        });
      }
  
      if ((typeof process === "undefined" ? "undefined" : (0, _typeof3.default)(process)) === "object" && process.domain) {
        invoke = process.domain.bind(invoke);
      }
  
      var invokeNext = invoke.bind(generator, "next");
      var invokeThrow = invoke.bind(generator, "throw");
      var invokeReturn = invoke.bind(generator, "return");
      var previousPromise;
  
      function enqueue(method, arg) {
        function callInvokeWithMethodAndArg() {
          return invoke(method, arg);
        }
  
        return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(callInvokeWithMethodAndArg,
        // Avoid propagating failures to Promises returned by later
        // invocations of the iterator.
        callInvokeWithMethodAndArg) : new _promise2.default(function (resolve) {
          resolve(callInvokeWithMethodAndArg());
        });
      }
  
      // Define the unified helper method that is used to implement .next,
      // .throw, and .return (see defineIteratorMethods).
      this._invoke = enqueue;
    }
  
    defineIteratorMethods(AsyncIterator.prototype);
  
    // Note that simple async functions are implemented on top of
    // AsyncIterator objects; they just return a Promise for the value of
    // the final result produced by the iterator.
    runtime.async = function (innerFn, outerFn, self, tryLocsList) {
      var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList));
  
      return runtime.isGeneratorFunction(outerFn) ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function (result) {
        return result.done ? result.value : iter.next();
      });
    };
  
    function makeInvokeMethod(innerFn, self, context) {
      var state = GenStateSuspendedStart;
  
      return function invoke(method, arg) {
        if (state === GenStateExecuting) {
          throw new Error("Generator is already running");
        }
  
        if (state === GenStateCompleted) {
          if (method === "throw") {
            throw arg;
          }
  
          // Be forgiving, per 25.3.3.3.3 of the spec:
          // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
          return doneResult();
        }
  
        while (true) {
          var delegate = context.delegate;
          if (delegate) {
            if (method === "return" || method === "throw" && delegate.iterator[method] === undefined) {
              // A return or throw (when the delegate iterator has no throw
              // method) always terminates the yield* loop.
              context.delegate = null;
  
              // If the delegate iterator has a return method, give it a
              // chance to clean up.
              var returnMethod = delegate.iterator["return"];
              if (returnMethod) {
                var record = tryCatch(returnMethod, delegate.iterator, arg);
                if (record.type === "throw") {
                  // If the return method threw an exception, let that
                  // exception prevail over the original return or throw.
                  method = "throw";
                  arg = record.arg;
                  continue;
                }
              }
  
              if (method === "return") {
                // Continue with the outer return, now that the delegate
                // iterator has been terminated.
                continue;
              }
            }
  
            var record = tryCatch(delegate.iterator[method], delegate.iterator, arg);
  
            if (record.type === "throw") {
              context.delegate = null;
  
              // Like returning generator.throw(uncaught), but without the
              // overhead of an extra function call.
              method = "throw";
              arg = record.arg;
              continue;
            }
  
            // Delegate generator ran and handled its own exceptions so
            // regardless of what the method was, we continue as if it is
            // "next" with an undefined arg.
            method = "next";
            arg = undefined;
  
            var info = record.arg;
            if (info.done) {
              context[delegate.resultName] = info.value;
              context.next = delegate.nextLoc;
            } else {
              state = GenStateSuspendedYield;
              return info;
            }
  
            context.delegate = null;
          }
  
          if (method === "next") {
            context._sent = arg;
  
            if (state === GenStateSuspendedYield) {
              context.sent = arg;
            } else {
              context.sent = undefined;
            }
          } else if (method === "throw") {
            if (state === GenStateSuspendedStart) {
              state = GenStateCompleted;
              throw arg;
            }
  
            if (context.dispatchException(arg)) {
              // If the dispatched exception was caught by a catch block,
              // then let that catch block handle the exception normally.
              method = "next";
              arg = undefined;
            }
          } else if (method === "return") {
            context.abrupt("return", arg);
          }
  
          state = GenStateExecuting;
  
          var record = tryCatch(innerFn, self, context);
          if (record.type === "normal") {
            // If an exception is thrown from innerFn, we leave state ===
            // GenStateExecuting and loop back for another invocation.
            state = context.done ? GenStateCompleted : GenStateSuspendedYield;
  
            var info = {
              value: record.arg,
              done: context.done
            };
  
            if (record.arg === ContinueSentinel) {
              if (context.delegate && method === "next") {
                // Deliberately forget the last sent value so that we don't
                // accidentally pass it on to the delegate.
                arg = undefined;
              }
            } else {
              return info;
            }
          } else if (record.type === "throw") {
            state = GenStateCompleted;
            // Dispatch the exception by looping back around to the
            // context.dispatchException(arg) call above.
            method = "throw";
            arg = record.arg;
          }
        }
      };
    }
  
    // Define Generator.prototype.{next,throw,return} in terms of the
    // unified ._invoke helper method.
    defineIteratorMethods(Gp);
  
    Gp[iteratorSymbol] = function () {
      return this;
    };
  
    Gp.toString = function () {
      return "[object Generator]";
    };
  
    function pushTryEntry(locs) {
      var entry = { tryLoc: locs[0] };
  
      if (1 in locs) {
        entry.catchLoc = locs[1];
      }
  
      if (2 in locs) {
        entry.finallyLoc = locs[2];
        entry.afterLoc = locs[3];
      }
  
      this.tryEntries.push(entry);
    }
  
    function resetTryEntry(entry) {
      var record = entry.completion || {};
      record.type = "normal";
      delete record.arg;
      entry.completion = record;
    }
  
    function Context(tryLocsList) {
      // The root entry object (effectively a try statement without a catch
      // or a finally block) gives us a place to store values thrown from
      // locations where there is no enclosing try statement.
      this.tryEntries = [{ tryLoc: "root" }];
      tryLocsList.forEach(pushTryEntry, this);
      this.reset(true);
    }
  
    runtime.keys = function (object) {
      var keys = [];
      for (var key in object) {
        keys.push(key);
      }
      keys.reverse();
  
      // Rather than returning an object with a next method, we keep
      // things simple and return the next function itself.
      return function next() {
        while (keys.length) {
          var key = keys.pop();
          if (key in object) {
            next.value = key;
            next.done = false;
            return next;
          }
        }
  
        // To avoid creating an additional object, we just hang the .value
        // and .done properties off the next function object itself. This
        // also ensures that the minifier will not anonymize the function.
        next.done = true;
        return next;
      };
    };
  
    function values(iterable) {
      if (iterable) {
        var iteratorMethod = iterable[iteratorSymbol];
        if (iteratorMethod) {
          return iteratorMethod.call(iterable);
        }
  
        if (typeof iterable.next === "function") {
          return iterable;
        }
  
        if (!isNaN(iterable.length)) {
          var i = -1,
              next = function next() {
            while (++i < iterable.length) {
              if (hasOwn.call(iterable, i)) {
                next.value = iterable[i];
                next.done = false;
                return next;
              }
            }
  
            next.value = undefined;
            next.done = true;
  
            return next;
          };
  
          return next.next = next;
        }
      }
  
      // Return an iterator with no values.
      return { next: doneResult };
    }
    runtime.values = values;
  
    function doneResult() {
      return { value: undefined, done: true };
    }
  
    Context.prototype = {
      constructor: Context,
  
      reset: function reset(skipTempReset) {
        this.prev = 0;
        this.next = 0;
        this.sent = undefined;
        this.done = false;
        this.delegate = null;
  
        this.tryEntries.forEach(resetTryEntry);
  
        if (!skipTempReset) {
          for (var name in this) {
            // Not sure about the optimal order of these conditions:
            if (name.charAt(0) === "t" && hasOwn.call(this, name) && !isNaN(+name.slice(1))) {
              this[name] = undefined;
            }
          }
        }
      },
  
      stop: function stop() {
        this.done = true;
  
        var rootEntry = this.tryEntries[0];
        var rootRecord = rootEntry.completion;
        if (rootRecord.type === "throw") {
          throw rootRecord.arg;
        }
  
        return this.rval;
      },
  
      dispatchException: function dispatchException(exception) {
        if (this.done) {
          throw exception;
        }
  
        var context = this;
        function handle(loc, caught) {
          record.type = "throw";
          record.arg = exception;
          context.next = loc;
          return !!caught;
        }
  
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          var record = entry.completion;
  
          if (entry.tryLoc === "root") {
            // Exception thrown outside of any try block that could handle
            // it, so set the completion value of the entire function to
            // throw the exception.
            return handle("end");
          }
  
          if (entry.tryLoc <= this.prev) {
            var hasCatch = hasOwn.call(entry, "catchLoc");
            var hasFinally = hasOwn.call(entry, "finallyLoc");
  
            if (hasCatch && hasFinally) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              } else if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else if (hasCatch) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              }
            } else if (hasFinally) {
              if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else {
              throw new Error("try statement without catch or finally");
            }
          }
        }
      },
  
      abrupt: function abrupt(type, arg) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
            var finallyEntry = entry;
            break;
          }
        }
  
        if (finallyEntry && (type === "break" || type === "continue") && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc) {
          // Ignore the finally entry if control is not jumping to a
          // location outside the try/catch block.
          finallyEntry = null;
        }
  
        var record = finallyEntry ? finallyEntry.completion : {};
        record.type = type;
        record.arg = arg;
  
        if (finallyEntry) {
          this.next = finallyEntry.finallyLoc;
        } else {
          this.complete(record);
        }
  
        return ContinueSentinel;
      },
  
      complete: function complete(record, afterLoc) {
        if (record.type === "throw") {
          throw record.arg;
        }
  
        if (record.type === "break" || record.type === "continue") {
          this.next = record.arg;
        } else if (record.type === "return") {
          this.rval = record.arg;
          this.next = "end";
        } else if (record.type === "normal" && afterLoc) {
          this.next = afterLoc;
        }
      },
  
      finish: function finish(finallyLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.finallyLoc === finallyLoc) {
            this.complete(entry.completion, entry.afterLoc);
            resetTryEntry(entry);
            return ContinueSentinel;
          }
        }
      },
  
      "catch": function _catch(tryLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.tryLoc === tryLoc) {
            var record = entry.completion;
            if (record.type === "throw") {
              var thrown = record.arg;
              resetTryEntry(entry);
            }
            return thrown;
          }
        }
  
        // The context.catch method must only be called with a location
        // argument that corresponds to a known catch block.
        throw new Error("illegal catch attempt");
      },
  
      delegateYield: function delegateYield(iterable, resultName, nextLoc) {
        this.delegate = {
          iterator: values(iterable),
          resultName: resultName,
          nextLoc: nextLoc
        };
  
        return ContinueSentinel;
      }
    };
  })(
  // Among the various tricks for obtaining a reference to the global
  // object, this seems to be the most reliable technique that does not
  // use indirect eval (which violates Content Security Policy).
  (typeof global === "undefined" ? "undefined" : (0, _typeof3.default)(global)) === "object" ? global : (typeof window === "undefined" ? "undefined" : (0, _typeof3.default)(window)) === "object" ? window : (typeof self === "undefined" ? "undefined" : (0, _typeof3.default)(self)) === "object" ? self : undefined);
  /* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(18)(module)))

/***/ },
/* 18 */
/***/ function(module, exports) {

  module.exports = function(module) {
  	if(!module.webpackPolyfill) {
  		module.deprecate = function() {};
  		module.paths = [];
  		// module.parent = undefined by default
  		module.children = [];
  		module.webpackPolyfill = 1;
  	}
  	return module;
  }


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(20), __esModule: true };

/***/ },
/* 20 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/promise");

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(22), __esModule: true };

/***/ },
/* 22 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/object/set-prototype-of");

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(24), __esModule: true };

/***/ },
/* 24 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/object/create");

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

  "use strict";
  
  exports.__esModule = true;
  
  var _symbol = __webpack_require__(26);
  
  var _symbol2 = _interopRequireDefault(_symbol);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function _typeof(obj) { return obj && typeof _Symbol !== "undefined" && obj.constructor === _Symbol ? "symbol" : typeof obj; }
  
  exports.default = function (obj) {
    return obj && typeof _symbol2.default !== "undefined" && obj.constructor === _symbol2.default ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
  };

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(27), __esModule: true };

/***/ },
/* 27 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/symbol");

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(29), __esModule: true };

/***/ },
/* 29 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/symbol/iterator");

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

  "use strict";
  
  exports.__esModule = true;
  
  var _promise = __webpack_require__(19);
  
  var _promise2 = _interopRequireDefault(_promise);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  exports.default = function (fn) {
    return function () {
      var gen = fn.apply(this, arguments);
      return new _promise2.default(function (resolve, reject) {
        function step(key, arg) {
          try {
            var info = gen[key](arg);
            var value = info.value;
          } catch (error) {
            reject(error);
            return;
          }
  
          if (info.done) {
            resolve(value);
          } else {
            _promise2.default.resolve(value).then(function (value) {
              step("next", value);
            }, function (err) {
              step("throw", err);
            });
          }
        }
  
        step("next");
      });
    };
  };

/***/ },
/* 31 */
/***/ function(module, exports) {

  "use strict";
  
  exports.__esModule = true;
  
  exports.default = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

  "use strict";
  
  exports.__esModule = true;
  
  var _defineProperty = __webpack_require__(33);
  
  var _defineProperty2 = _interopRequireDefault(_defineProperty);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  exports.default = (function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        (0, _defineProperty2.default)(target, descriptor.key, descriptor);
      }
    }
  
    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  })();

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

  module.exports = { "default": __webpack_require__(34), __esModule: true };

/***/ },
/* 34 */
/***/ function(module, exports) {

  module.exports = require("core-js/library/fn/object/define-property");

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _classCallCheck2 = __webpack_require__(31);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(32);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _pathToRegexp = __webpack_require__(36);
  
  var _pathToRegexp2 = _interopRequireDefault(_pathToRegexp);
  
  var _Match = __webpack_require__(38);
  
  var _Match2 = _interopRequireDefault(_Match);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Routing | http://www.kriasoft.com/react-routing
   * Copyright (c) Konstantin Tarkus <hello@tarkus.me> | The MIT License
   */
  
  var Route = function () {
    function Route(path, handlers) {
      (0, _classCallCheck3.default)(this, Route);
  
      this.path = path;
      this.handlers = handlers;
      this.regExp = (0, _pathToRegexp2.default)(path, this.keys = []);
    }
  
    (0, _createClass3.default)(Route, [{
      key: 'match',
      value: function match(path) {
        var m = this.regExp.exec(path);
        return m ? new _Match2.default(this, path, this.keys, m) : null;
      }
    }]);
    return Route;
  }();
  
  exports.default = Route;

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

  var isarray = __webpack_require__(37)
  
  /**
   * Expose `pathToRegexp`.
   */
  module.exports = pathToRegexp
  module.exports.parse = parse
  module.exports.compile = compile
  module.exports.tokensToFunction = tokensToFunction
  module.exports.tokensToRegExp = tokensToRegExp
  
  /**
   * The main path matching regexp utility.
   *
   * @type {RegExp}
   */
  var PATH_REGEXP = new RegExp([
    // Match escaped characters that would otherwise appear in future matches.
    // This allows the user to escape special characters that won't transform.
    '(\\\\.)',
    // Match Express-style parameters and un-named parameters with a prefix
    // and optional suffixes. Matches appear as:
    //
    // "/:test(\\d+)?" => ["/", "test", "\d+", undefined, "?", undefined]
    // "/route(\\d+)"  => [undefined, undefined, undefined, "\d+", undefined, undefined]
    // "/*"            => ["/", undefined, undefined, undefined, undefined, "*"]
    '([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^()])+)\\))?|\\(((?:\\\\.|[^()])+)\\))([+*?])?|(\\*))'
  ].join('|'), 'g')
  
  /**
   * Parse a string for the raw tokens.
   *
   * @param  {String} str
   * @return {Array}
   */
  function parse (str) {
    var tokens = []
    var key = 0
    var index = 0
    var path = ''
    var res
  
    while ((res = PATH_REGEXP.exec(str)) != null) {
      var m = res[0]
      var escaped = res[1]
      var offset = res.index
      path += str.slice(index, offset)
      index = offset + m.length
  
      // Ignore already escaped sequences.
      if (escaped) {
        path += escaped[1]
        continue
      }
  
      // Push the current path onto the tokens.
      if (path) {
        tokens.push(path)
        path = ''
      }
  
      var prefix = res[2]
      var name = res[3]
      var capture = res[4]
      var group = res[5]
      var suffix = res[6]
      var asterisk = res[7]
  
      var repeat = suffix === '+' || suffix === '*'
      var optional = suffix === '?' || suffix === '*'
      var delimiter = prefix || '/'
      var pattern = capture || group || (asterisk ? '.*' : '[^' + delimiter + ']+?')
  
      tokens.push({
        name: name || key++,
        prefix: prefix || '',
        delimiter: delimiter,
        optional: optional,
        repeat: repeat,
        pattern: escapeGroup(pattern)
      })
    }
  
    // Match any characters still remaining.
    if (index < str.length) {
      path += str.substr(index)
    }
  
    // If the path exists, push it onto the end.
    if (path) {
      tokens.push(path)
    }
  
    return tokens
  }
  
  /**
   * Compile a string to a template function for the path.
   *
   * @param  {String}   str
   * @return {Function}
   */
  function compile (str) {
    return tokensToFunction(parse(str))
  }
  
  /**
   * Expose a method for transforming tokens into the path function.
   */
  function tokensToFunction (tokens) {
    // Compile all the tokens into regexps.
    var matches = new Array(tokens.length)
  
    // Compile all the patterns before compilation.
    for (var i = 0; i < tokens.length; i++) {
      if (typeof tokens[i] === 'object') {
        matches[i] = new RegExp('^' + tokens[i].pattern + '$')
      }
    }
  
    return function (obj) {
      var path = ''
      var data = obj || {}
  
      for (var i = 0; i < tokens.length; i++) {
        var token = tokens[i]
  
        if (typeof token === 'string') {
          path += token
  
          continue
        }
  
        var value = data[token.name]
        var segment
  
        if (value == null) {
          if (token.optional) {
            continue
          } else {
            throw new TypeError('Expected "' + token.name + '" to be defined')
          }
        }
  
        if (isarray(value)) {
          if (!token.repeat) {
            throw new TypeError('Expected "' + token.name + '" to not repeat, but received "' + value + '"')
          }
  
          if (value.length === 0) {
            if (token.optional) {
              continue
            } else {
              throw new TypeError('Expected "' + token.name + '" to not be empty')
            }
          }
  
          for (var j = 0; j < value.length; j++) {
            segment = encodeURIComponent(value[j])
  
            if (!matches[i].test(segment)) {
              throw new TypeError('Expected all "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
            }
  
            path += (j === 0 ? token.prefix : token.delimiter) + segment
          }
  
          continue
        }
  
        segment = encodeURIComponent(value)
  
        if (!matches[i].test(segment)) {
          throw new TypeError('Expected "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
        }
  
        path += token.prefix + segment
      }
  
      return path
    }
  }
  
  /**
   * Escape a regular expression string.
   *
   * @param  {String} str
   * @return {String}
   */
  function escapeString (str) {
    return str.replace(/([.+*?=^!:${}()[\]|\/])/g, '\\$1')
  }
  
  /**
   * Escape the capturing group by escaping special characters and meaning.
   *
   * @param  {String} group
   * @return {String}
   */
  function escapeGroup (group) {
    return group.replace(/([=!:$\/()])/g, '\\$1')
  }
  
  /**
   * Attach the keys as a property of the regexp.
   *
   * @param  {RegExp} re
   * @param  {Array}  keys
   * @return {RegExp}
   */
  function attachKeys (re, keys) {
    re.keys = keys
    return re
  }
  
  /**
   * Get the flags for a regexp from the options.
   *
   * @param  {Object} options
   * @return {String}
   */
  function flags (options) {
    return options.sensitive ? '' : 'i'
  }
  
  /**
   * Pull out keys from a regexp.
   *
   * @param  {RegExp} path
   * @param  {Array}  keys
   * @return {RegExp}
   */
  function regexpToRegexp (path, keys) {
    // Use a negative lookahead to match only capturing groups.
    var groups = path.source.match(/\((?!\?)/g)
  
    if (groups) {
      for (var i = 0; i < groups.length; i++) {
        keys.push({
          name: i,
          prefix: null,
          delimiter: null,
          optional: false,
          repeat: false,
          pattern: null
        })
      }
    }
  
    return attachKeys(path, keys)
  }
  
  /**
   * Transform an array into a regexp.
   *
   * @param  {Array}  path
   * @param  {Array}  keys
   * @param  {Object} options
   * @return {RegExp}
   */
  function arrayToRegexp (path, keys, options) {
    var parts = []
  
    for (var i = 0; i < path.length; i++) {
      parts.push(pathToRegexp(path[i], keys, options).source)
    }
  
    var regexp = new RegExp('(?:' + parts.join('|') + ')', flags(options))
  
    return attachKeys(regexp, keys)
  }
  
  /**
   * Create a path regexp from string input.
   *
   * @param  {String} path
   * @param  {Array}  keys
   * @param  {Object} options
   * @return {RegExp}
   */
  function stringToRegexp (path, keys, options) {
    var tokens = parse(path)
    var re = tokensToRegExp(tokens, options)
  
    // Attach keys back to the regexp.
    for (var i = 0; i < tokens.length; i++) {
      if (typeof tokens[i] !== 'string') {
        keys.push(tokens[i])
      }
    }
  
    return attachKeys(re, keys)
  }
  
  /**
   * Expose a function for taking tokens and returning a RegExp.
   *
   * @param  {Array}  tokens
   * @param  {Array}  keys
   * @param  {Object} options
   * @return {RegExp}
   */
  function tokensToRegExp (tokens, options) {
    options = options || {}
  
    var strict = options.strict
    var end = options.end !== false
    var route = ''
    var lastToken = tokens[tokens.length - 1]
    var endsWithSlash = typeof lastToken === 'string' && /\/$/.test(lastToken)
  
    // Iterate over the tokens and create our regexp string.
    for (var i = 0; i < tokens.length; i++) {
      var token = tokens[i]
  
      if (typeof token === 'string') {
        route += escapeString(token)
      } else {
        var prefix = escapeString(token.prefix)
        var capture = token.pattern
  
        if (token.repeat) {
          capture += '(?:' + prefix + capture + ')*'
        }
  
        if (token.optional) {
          if (prefix) {
            capture = '(?:' + prefix + '(' + capture + '))?'
          } else {
            capture = '(' + capture + ')?'
          }
        } else {
          capture = prefix + '(' + capture + ')'
        }
  
        route += capture
      }
    }
  
    // In non-strict mode we allow a slash at the end of match. If the path to
    // match already ends with a slash, we remove it for consistency. The slash
    // is valid at the end of a path match, not in the middle. This is important
    // in non-ending mode, where "/test/" shouldn't match "/test//route".
    if (!strict) {
      route = (endsWithSlash ? route.slice(0, -2) : route) + '(?:\\/(?=$))?'
    }
  
    if (end) {
      route += '$'
    } else {
      // In non-ending mode, we need the capturing groups to match as much as
      // possible by using a positive lookahead to the end or next path segment.
      route += strict && endsWithSlash ? '' : '(?=\\/|$)'
    }
  
    return new RegExp('^' + route, flags(options))
  }
  
  /**
   * Normalize the given path string, returning a regular expression.
   *
   * An empty array can be passed in for the keys, which will hold the
   * placeholder key descriptions. For example, using `/user/:id`, `keys` will
   * contain `[{ name: 'id', delimiter: '/', optional: false, repeat: false }]`.
   *
   * @param  {(String|RegExp|Array)} path
   * @param  {Array}                 [keys]
   * @param  {Object}                [options]
   * @return {RegExp}
   */
  function pathToRegexp (path, keys, options) {
    keys = keys || []
  
    if (!isarray(keys)) {
      options = keys
      keys = []
    } else if (!options) {
      options = {}
    }
  
    if (path instanceof RegExp) {
      return regexpToRegexp(path, keys, options)
    }
  
    if (isarray(path)) {
      return arrayToRegexp(path, keys, options)
    }
  
    return stringToRegexp(path, keys, options)
  }


/***/ },
/* 37 */
/***/ function(module, exports) {

  module.exports = Array.isArray || function (arr) {
    return Object.prototype.toString.call(arr) == '[object Array]';
  };


/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _create = __webpack_require__(23);
  
  var _create2 = _interopRequireDefault(_create);
  
  var _classCallCheck2 = __webpack_require__(31);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Routing | http://www.kriasoft.com/react-routing
   * Copyright (c) Konstantin Tarkus <hello@tarkus.me> | The MIT License
   */
  
  var Match = function Match(route, path, keys, match) {
    (0, _classCallCheck3.default)(this, Match);
  
    this.route = route;
    this.path = path;
    this.params = (0, _create2.default)(null);
    for (var i = 1; i < match.length; i++) {
      this.params[keys[i - 1].name] = decodeParam(match[i]);
    }
  };
  
  function decodeParam(val) {
    if (!(typeof val === 'string' || val instanceof String)) {
      return val;
    }
  
    try {
      return decodeURIComponent(val);
    } catch (e) {
      var err = new TypeError('Failed to decode param \'' + val + '\'');
      err.status = 400;
      throw err;
    }
  }
  
  exports.default = Match;

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Response = exports.Headers = exports.Request = exports.default = undefined;
  
  var _nodeFetch = __webpack_require__(40);
  
  var _nodeFetch2 = _interopRequireDefault(_nodeFetch);
  
  var _config = __webpack_require__(41);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  function localUrl(url) {
    if (url.startsWith('//')) {
      return 'https:' + url;
    }
  
    if (url.startsWith('http')) {
      return url;
    }
  
    return 'http://' + _config.host + url;
  }
  
  function localFetch(url, options) {
    return (0, _nodeFetch2.default)(localUrl(url), options);
  }
  
  exports.default = localFetch;
  exports.Request = _nodeFetch.Request;
  exports.Headers = _nodeFetch.Headers;
  exports.Response = _nodeFetch.Response;

/***/ },
/* 40 */
/***/ function(module, exports) {

  module.exports = require("node-fetch");

/***/ },
/* 41 */
/***/ function(module, exports) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var port = exports.port = process.env.PORT || 5000;
  var mongo_db = exports.mongo_db = process.env.MONGODB_URL || 'mongodb://localhost/finpal';
  var host = exports.host = process.env.WEBSITE_HOSTNAME || 'localhost:' + port;
  var googleAnalyticsId = exports.googleAnalyticsId = 'UA-XXXXX-X';

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _emptyFunction = __webpack_require__(48);
  
  var _emptyFunction2 = _interopRequireDefault(_emptyFunction);
  
  var _App = __webpack_require__(49);
  
  var _App2 = _interopRequireDefault(_App);
  
  var _Header = __webpack_require__(57);
  
  var _Header2 = _interopRequireDefault(_Header);
  
  var _Feedback = __webpack_require__(75);
  
  var _Feedback2 = _interopRequireDefault(_Feedback);
  
  var _Footer = __webpack_require__(78);
  
  var _Footer2 = _interopRequireDefault(_Footer);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var App = function (_Component) {
    (0, _inherits3.default)(App, _Component);
  
    function App() {
      (0, _classCallCheck3.default)(this, App);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(App).apply(this, arguments));
    }
  
    (0, _createClass3.default)(App, [{
      key: 'getChildContext',
      value: function getChildContext() {
        var context = this.props.context;
        return {
          insertCss: context.insertCss || _emptyFunction2.default,
          onSetTitle: context.onSetTitle || _emptyFunction2.default,
          onSetMeta: context.onSetMeta || _emptyFunction2.default,
          onPageNotFound: context.onPageNotFound || _emptyFunction2.default
        };
      }
    }, {
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.removeCss = this.props.context.insertCss(_App2.default);
      }
    }, {
      key: 'componentWillUnmount',
      value: function componentWillUnmount() {
        this.removeCss();
      }
    }, {
      key: 'render',
      value: function render() {
        return !this.props.error ? _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(_Header2.default, null),
          this.props.children,
          _react2.default.createElement(_Feedback2.default, null),
          _react2.default.createElement(_Footer2.default, null)
        ) : this.props.children;
      }
    }]);
    return App;
  }(_react.Component);
  
  App.propTypes = {
    context: _react.PropTypes.shape({
      insertCss: _react.PropTypes.func,
      onSetTitle: _react.PropTypes.func,
      onSetMeta: _react.PropTypes.func,
      onPageNotFound: _react.PropTypes.func
    }),
    children: _react.PropTypes.element.isRequired,
    error: _react.PropTypes.object
  };
  App.childContextTypes = {
    insertCss: _react.PropTypes.func.isRequired,
    onSetTitle: _react.PropTypes.func.isRequired,
    onSetMeta: _react.PropTypes.func.isRequired,
    onPageNotFound: _react.PropTypes.func.isRequired
  };
  exports.default = App;

/***/ },
/* 43 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/core-js/object/get-prototype-of");

/***/ },
/* 44 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/classCallCheck");

/***/ },
/* 45 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/createClass");

/***/ },
/* 46 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/possibleConstructorReturn");

/***/ },
/* 47 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/inherits");

/***/ },
/* 48 */
/***/ function(module, exports) {

  module.exports = require("fbjs/lib/emptyFunction");

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(50);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  exports.push([module.id, "@import url(http://cloud.webtype.com/css/9d654ee3-7078-4fa5-9d5d-2000db9a3cc4.css);", ""]);
  
  // module
  exports.push([module.id, "\n/*! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css */html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:700}dfn{font-style:italic}h1{font-size:2em;margin:.67em 0}mark{background:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{box-sizing:content-box;height:0}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace;font-size:1em}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}button{overflow:visible}button,select{text-transform:none}button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type=checkbox],input[type=radio]{box-sizing:border-box;padding:0}input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{height:auto}input[type=search]{-webkit-appearance:textfield;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{border:0;padding:0}textarea{overflow:auto}optgroup{font-weight:700}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}/*/*//*/*//html{color:#222;font-weight:400;font-size:1em;line-height:1.375;font-weight:700}/*/*//*/*//html,body,p,strong{font-family:Benton Sans Book,Helvetica,sans-serif}h1,h2,h3,h4,h5{font-family:Benton Sans,Helvetica,sans-serif;font-weight:700}body{margin:0}::-moz-selection{background:#b3d4fc;text-shadow:none}::selection{background:#b3d4fc;text-shadow:none}hr{display:block;height:1px;border:0;border-top:1px solid #ccc;margin:1em 0;padding:0}audio,canvas,iframe,img,svg,video{vertical-align:middle}fieldset{border:0;margin:0;padding:0}textarea{resize:vertical}.browserupgrade{margin:.2em 0;background:#ccc;color:#000;padding:.2em 0}@media print{*,:after,:before{background:transparent!important;color:#000!important;box-shadow:none!important;text-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:\" (\" attr(href) \")\"}abbr[title]:after{content:\" (\" attr(title) \")\"}a[href^=\"#\"]:after,a[href^=\"javascript:\"]:after{content:\"\"}blockquote,pre{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}img,tr{page-break-inside:avoid}img{max-width:100%!important}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}}", ""]);
  
  // exports


/***/ },
/* 51 */
/***/ function(module, exports) {

  /*
  	MIT License http://www.opensource.org/licenses/mit-license.php
  	Author Tobias Koppers @sokra
  */
  // css base code, injected by the css-loader
  module.exports = function() {
  	var list = [];
  
  	// return the list of modules as css string
  	list.toString = function toString() {
  		var result = [];
  		for(var i = 0; i < this.length; i++) {
  			var item = this[i];
  			if(item[2]) {
  				result.push("@media " + item[2] + "{" + item[1] + "}");
  			} else {
  				result.push(item[1]);
  			}
  		}
  		return result.join("");
  	};
  
  	// import a list of modules into the list
  	list.i = function(modules, mediaQuery) {
  		if(typeof modules === "string")
  			modules = [[null, modules, ""]];
  		var alreadyImportedModules = {};
  		for(var i = 0; i < this.length; i++) {
  			var id = this[i][0];
  			if(typeof id === "number")
  				alreadyImportedModules[id] = true;
  		}
  		for(i = 0; i < modules.length; i++) {
  			var item = modules[i];
  			// skip already imported module
  			// this implementation is not 100% perfect for weird media query combinations
  			//  when a module is imported multiple times with different media queries.
  			//  I hope this will never occur (Hey this way we have smaller bundles)
  			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
  				if(mediaQuery && !item[2]) {
  					item[2] = mediaQuery;
  				} else if(mediaQuery) {
  					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
  				}
  				list.push(item);
  			}
  		}
  	};
  	return list;
  };


/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _stringify = __webpack_require__(53);
  
  var _stringify2 = _interopRequireDefault(_stringify);
  
  var _slicedToArray2 = __webpack_require__(54);
  
  var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);
  
  var _assign = __webpack_require__(55);
  
  var _assign2 = _interopRequireDefault(_assign);
  
  var _getIterator2 = __webpack_require__(56);
  
  var _getIterator3 = _interopRequireDefault(_getIterator2);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * Isomorphic CSS style loader for Webpack
   *
   * Copyright © 2015 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var prefix = 's';
  var inserted = {};
  var canUseURL = typeof URL === 'function' && typeof URL.createObjectURL === 'function' && typeof URL.revokeObjectURL === 'function' && typeof Blob === 'function' && typeof btoa === 'function';
  
  // Base64 encoding and decoding - The "Unicode Problem"
  // https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem
  function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
      return String.fromCharCode('0x' + p1);
    }));
  }
  
  /**
   * Remove style/link elements for specified Module IDs
   * if they are no longer referenced by UI components.
   */
  function removeCss(ids) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;
  
    try {
      for (var _iterator = (0, _getIterator3.default)(ids), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var id = _step.value;
  
        if (--inserted[id] <= 0) {
          var elem = document.getElementById(prefix + id);
          if (elem) {
            elem.parentNode.removeChild(elem);
            if (canUseURL && elem.tagName === 'LINK' && elem.href) {
              URL.revokeObjectURL(elem.href);
            }
          }
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }
  
  /**
   * Example:
   *   // Insert CSS styles object generated by `css-loader` into DOM
   *   var removeCss = insertCss([[1, 'body { color: red; }']]);
   *
   *   // Remove it from the DOM
   *   removeCss();
   */
  function insertCss(styles, options) {
    var _Object$assign = (0, _assign2.default)({
      replace: false,
      prepend: false
    }, options);
  
    var replace = _Object$assign.replace;
    var prepend = _Object$assign.prepend;
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;
  
    try {
  
      for (var _iterator2 = (0, _getIterator3.default)(styles), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var _step2$value = (0, _slicedToArray3.default)(_step2.value, 4);
  
        var id = _step2$value[0];
        var css = _step2$value[1];
        var media = _step2$value[2];
        var sourceMap = _step2$value[3];
  
        if (inserted[id]) {
          if (!replace) {
            inserted[id]++;
            continue;
          }
        }
  
        inserted[id] = 1;
  
        var elem = document.getElementById(prefix + id);
        var create = false;
  
        if (!elem) {
          create = true;
  
          if (sourceMap && canUseURL) {
            elem = document.createElement('link');
            elem.setAttribute('rel', 'stylesheet');
          } else {
            elem = document.createElement('style');
            elem.setAttribute('type', 'text/css');
          }
  
          elem.id = prefix + id;
  
          if (media) {
            elem.setAttribute('media', media);
          }
        }
  
        if (elem.tagName === 'STYLE') {
          if ('textContent' in elem) {
            elem.textContent = css;
          } else {
            elem.styleSheet.cssText = css;
          }
        } else {
          var blob = new Blob([css + '\n/*# sourceMappingURL=data:application/json;base64,' + (b64EncodeUnicode((0, _stringify2.default)(sourceMap)) + ' */')], { type: 'text/css' });
  
          var href = elem.href;
          elem.href = URL.createObjectURL(blob);
  
          if (href) {
            URL.revokeObjectURL(href);
          }
        }
  
        if (create) {
          if (prepend) {
            document.head.insertBefore(elem, document.head.childNodes[0]);
          } else {
            document.head.appendChild(elem);
          }
        }
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }
  
    return removeCss.bind(null, styles.map(function (x) {
      return x[0];
    }));
  }
  
  module.exports = insertCss;

/***/ },
/* 53 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/core-js/json/stringify");

/***/ },
/* 54 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/slicedToArray");

/***/ },
/* 55 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/core-js/object/assign");

/***/ },
/* 56 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/core-js/get-iterator");

/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _Header = __webpack_require__(59);
  
  var _Header2 = _interopRequireDefault(_Header);
  
  var _Link = __webpack_require__(61);
  
  var _Link2 = _interopRequireDefault(_Link);
  
  var _Navigation = __webpack_require__(70);
  
  var _Navigation2 = _interopRequireDefault(_Navigation);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var Header = function (_Component) {
    (0, _inherits3.default)(Header, _Component);
  
    function Header() {
      (0, _classCallCheck3.default)(this, Header);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(Header).apply(this, arguments));
    }
  
    (0, _createClass3.default)(Header, [{
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: _Header2.default.root },
          _react2.default.createElement(
            'div',
            { className: _Header2.default.container },
            _react2.default.createElement(_Navigation2.default, { className: _Header2.default.nav })
          )
        );
      }
    }]);
    return Header;
  }(_react.Component); /**
                        * React Starter Kit (https://www.reactstarterkit.com/)
                        *
                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                        *
                        * This source code is licensed under the MIT license found in the
                        * LICENSE.txt file in the root directory of this source tree.
                        */
  
  exports.default = (0, _withStyles2.default)(Header, _Header2.default);

/***/ },
/* 58 */
/***/ function(module, exports) {

  module.exports = require("isomorphic-style-loader/lib/withStyles");

/***/ },
/* 59 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(60);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._14IZ{color:#373277;position:absolute;top:0;height:auto;z-index:1000;width:100%}.izfM{margin:0 auto;padding:20px 0;max-width:1000px}._1-TO{color:#92e5fc;text-decoration:none;font-size:1.75em}._162t{margin-left:10px}._3wxE{margin-top:6px}.UgCI{text-align:center}._3Qi2{margin:0;padding:10px;font-weight:400;font-size:4em;line-height:1em}._3OwM{padding:0;color:hsla(0,0%,100%,.5);font-size:1.25em;margin:0}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_14IZ",
  	"container": "izfM",
  	"brand": "_1-TO",
  	"brandTxt": "_162t",
  	"nav": "_3wxE",
  	"banner": "UgCI",
  	"bannerTitle": "_3Qi2",
  	"bannerDesc": "_3OwM"
  };

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _objectWithoutProperties2 = __webpack_require__(62);
  
  var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);
  
  var _extends2 = __webpack_require__(63);
  
  var _extends3 = _interopRequireDefault(_extends2);
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _parsePath = __webpack_require__(64);
  
  var _parsePath2 = _interopRequireDefault(_parsePath);
  
  var _Location = __webpack_require__(65);
  
  var _Location2 = _interopRequireDefault(_Location);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function isLeftClickEvent(event) {
    return event.button === 0;
  } /**
     * React Starter Kit (https://www.reactstarterkit.com/)
     *
     * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE.txt file in the root directory of this source tree.
     */
  
  function isModifiedEvent(event) {
    return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
  }
  
  var Link = function (_Component) {
    (0, _inherits3.default)(Link, _Component);
  
    function Link() {
      var _Object$getPrototypeO;
  
      var _temp, _this, _ret;
  
      (0, _classCallCheck3.default)(this, Link);
  
      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
  
      return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_Object$getPrototypeO = (0, _getPrototypeOf2.default)(Link)).call.apply(_Object$getPrototypeO, [this].concat(args))), _this), _this.handleClick = function (event) {
        var allowTransition = true;
        var clickResult = undefined;
  
        if (_this.props && _this.props.onClick) {
          clickResult = _this.props.onClick(event);
        }
  
        if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
          return;
        }
  
        if (clickResult === false || event.defaultPrevented === true) {
          allowTransition = false;
        }
  
        event.preventDefault();
  
        if (allowTransition) {
          var link = event.currentTarget;
          if (_this.props && _this.props.to) {
            _Location2.default.push((0, _extends3.default)({}, (0, _parsePath2.default)(_this.props.to), {
              state: _this.props && _this.props.state || null
            }));
          } else {
            _Location2.default.push({
              pathname: link.pathname,
              search: link.search,
              state: _this.props && _this.props.state || null
            });
          }
        }
      }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
    }
  
    (0, _createClass3.default)(Link, [{
      key: 'render',
      value: function render() {
        var _props = this.props;
        var to = _props.to;
        var query = _props.query;
        var props = (0, _objectWithoutProperties3.default)(_props, ['to', 'query']);
  
        return _react2.default.createElement('a', (0, _extends3.default)({ href: _Location2.default.createHref(to, query) }, props, { onClick: this.handleClick }));
      }
    }]);
    return Link;
  }(_react.Component);
  
  Link.propTypes = {
    to: _react.PropTypes.string.isRequired,
    query: _react.PropTypes.object,
    state: _react.PropTypes.object,
    onClick: _react.PropTypes.func
  };
  exports.default = Link;

/***/ },
/* 62 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/objectWithoutProperties");

/***/ },
/* 63 */
/***/ function(module, exports) {

  module.exports = require("babel-runtime/helpers/extends");

/***/ },
/* 64 */
/***/ function(module, exports) {

  module.exports = require("history/lib/parsePath");

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _ExecutionEnvironment = __webpack_require__(66);
  
  var _createBrowserHistory = __webpack_require__(67);
  
  var _createBrowserHistory2 = _interopRequireDefault(_createBrowserHistory);
  
  var _createMemoryHistory = __webpack_require__(68);
  
  var _createMemoryHistory2 = _interopRequireDefault(_createMemoryHistory);
  
  var _useQueries = __webpack_require__(69);
  
  var _useQueries2 = _interopRequireDefault(_useQueries);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var location = (0, _useQueries2.default)(_ExecutionEnvironment.canUseDOM ? _createBrowserHistory2.default : _createMemoryHistory2.default)();
  
  exports.default = location;

/***/ },
/* 66 */
/***/ function(module, exports) {

  module.exports = require("fbjs/lib/ExecutionEnvironment");

/***/ },
/* 67 */
/***/ function(module, exports) {

  module.exports = require("history/lib/createBrowserHistory");

/***/ },
/* 68 */
/***/ function(module, exports) {

  module.exports = require("history/lib/createMemoryHistory");

/***/ },
/* 69 */
/***/ function(module, exports) {

  module.exports = require("history/lib/useQueries");

/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _classnames = __webpack_require__(71);
  
  var _classnames2 = _interopRequireDefault(_classnames);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _Navigation = __webpack_require__(72);
  
  var _Navigation2 = _interopRequireDefault(_Navigation);
  
  var _Link = __webpack_require__(61);
  
  var _Link2 = _interopRequireDefault(_Link);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var Navigation = function (_Component) {
    (0, _inherits3.default)(Navigation, _Component);
  
    function Navigation() {
      (0, _classCallCheck3.default)(this, Navigation);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(Navigation).apply(this, arguments));
    }
  
    (0, _createClass3.default)(Navigation, [{
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)(_Navigation2.default.root, this.props.className), role: 'navigation' },
          _react2.default.createElement(
            'div',
            { className: 'contain' },
            _react2.default.createElement(
              'div',
              { className: _Navigation2.default.menuLeft },
              _react2.default.createElement(
                'div',
                { className: _Navigation2.default.Menu },
                _react2.default.createElement(
                  'ul',
                  { className: _Navigation2.default.MenuList },
                  _react2.default.createElement(
                    'li',
                    { className: _Navigation2.default.MenuItem },
                    _react2.default.createElement(
                      _Link2.default,
                      { className: _Navigation2.default.MenuLink, to: '/' },
                      _react2.default.createElement('img', { src: __webpack_require__(74), alt: 'The Finpal', width: '100px' })
                    )
                  ),
                  _react2.default.createElement(
                    'a',
                    { href: '#', className: 'custom-toggle', id: 'toggle' },
                    _react2.default.createElement('s', { className: 'bar' }),
                    _react2.default.createElement('s', { className: 'bar' })
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: _Navigation2.default.menuRight },
              _react2.default.createElement(
                'ul',
                { className: _Navigation2.default.MenuList },
                _react2.default.createElement(
                  'li',
                  { className: _Navigation2.default.MenuItem },
                  _react2.default.createElement(
                    _Link2.default,
                    { className: _Navigation2.default.MenuLink, to: '/about' },
                    'About'
                  )
                ),
                _react2.default.createElement(
                  'li',
                  { className: _Navigation2.default.MenuItem },
                  _react2.default.createElement(
                    'a',
                    { href: '#services', className: _Navigation2.default.MenuLink },
                    'Our Services'
                  )
                ),
                _react2.default.createElement(
                  'li',
                  { className: _Navigation2.default.MenuItem },
                  _react2.default.createElement(
                    'a',
                    { target: '_blank', href: 'http://blog.thefinpal.com/', className: _Navigation2.default.MenuLink },
                    'Blog'
                  )
                )
              )
            )
          )
        );
      }
    }]);
    return Navigation;
  }(_react.Component); /**
                        * React Starter Kit (https://www.reactstarterkit.com/)
                        *
                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                        *
                        * This source code is licensed under the MIT license found in the
                        * LICENSE.txt file in the root directory of this source tree.
                        */
  
  Navigation.propTypes = {
    className: _react.PropTypes.string
  };
  exports.default = (0, _withStyles2.default)(Navigation, _Navigation2.default);

/***/ },
/* 71 */
/***/ function(module, exports) {

  module.exports = require("classnames");

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(73);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 73 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._12ku{display:inline-block;padding:3px 8px;text-decoration:none;font-size:1.125em}._12ku,._12ku:active,._12ku:visited{color:hsla(0,0%,100%,.6)}._2cuA,._12ku:hover{color:#fff}._2cuA{margin-right:8px;margin-left:8px;border-radius:3px;background:rgba(0,0,0,.15)}._2cuA:hover{background:rgba(0,0,0,.3)}._2MVO{color:hsla(0,0%,100%,.3)}._11By{width:23.72881%;float:left;margin-right:1.69492%}._15MK{width:74.57627%;float:right;margin-right:0;display:block}._2CvK{box-sizing:border-box}._8_9k{list-style:none;margin:0;padding-top:1.5rem}._8_9k,.FxNX{position:relative;padding:0}.FxNX{float:left}._2UGJ,.FxNX{height:2em;line-height:2em}._2UGJ{display:inline-block;width:100%;text-decoration:none;white-space:nowrap;font-family:Benton Sans,Helvetica,sans-serif;padding:1.2rem;vertical-align:top;zoom:1;color:#453b4c;*display:inline;padding-top:0}._2UGJ:hover{color:#666}._2YDT{display:block;text-decoration:none}._2VqL,._2YDT{white-space:nowrap}._2VqL{width:100%}._2VqL>._1Lhw{display:inline-block}._2VqL>.FxNX{display:inline-block;*display:inline;zoom:1;vertical-align:middle}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_2Gxu",
  	"link": "_12ku",
  	"highlight": "_2cuA",
  	"spacer": "_2MVO",
  	"menuLeft": "_11By",
  	"menuRight": "_15MK",
  	"Menu": "_2CvK",
  	"MenuList": "_8_9k",
  	"MenuItem": "FxNX",
  	"MenuLink": "_2UGJ",
  	"MenuHeading": "_2YDT",
  	"Menu--horizontal": "_2VqL",
  	"Menu__List": "_1Lhw"
  };

/***/ },
/* 74 */
/***/ function(module, exports) {

  module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+Cjxzdmcgd2lkdGg9IjI1M3B4IiBoZWlnaHQ9IjkwcHgiIHZpZXdCb3g9IjAgMCAyNTMgOTAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiPgogICAgPCEtLSBHZW5lcmF0b3I6IFNrZXRjaCAzLjMuMiAoMTIwNDMpIC0gaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoIC0tPgogICAgPHRpdGxlPkZpbnBhbCBMb2dvPC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc2tldGNoOnR5cGU9Ik1TUGFnZSI+CiAgICAgICAgPGcgaWQ9IkZpbnBhbC1Mb2dvIiBza2V0Y2g6dHlwZT0iTVNMYXllckdyb3VwIj4KICAgICAgICAgICAgPHBhdGggZD0iTTI1Mi42NjQsMCBMMjUyLjY2NCw2OS43OTEgTDIzOSw2OS43OTEgTDIzOSwxMS43NDQgTDI1Mi42NjQsMCIgaWQ9IkZpbGwtNzkiIGZpbGw9IiM0RDI5NjYiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiPjwvcGF0aD4KICAgICAgICAgICAgPHBhdGggZD0iTTYxLjU3Miw5LjQwNyBMNjEuNTczLDkuNDA2IEw1NS42NjgsMC42ODMgTDU1LjY2NSwwLjY4MyBDNTUuNDAxLDAuMjczIDU0Ljk0MiwwIDU0LjQxOCwwIEM1My44OTMsMCA1My40MzQsMC4yNzMgNTMuMTcsMC42ODMgTDUzLjE2NywwLjY4MyBMNDcuMjc4LDkuMzgzIEw0Ny4yODIsOS4zODQgQzQ3LjEwNyw5LjYyNyA0Nyw5LjkyNSA0NywxMC4yNDkgQzQ3LDExLjA2OSA0Ny42NjQsMTEuNzM0IDQ4LjQ4NCwxMS43MzQgTDYwLjM1LDExLjczNCBDNjEuMTcxLDExLjczNCA2MS44MzYsMTEuMDY5IDYxLjgzNiwxMC4yNDkgQzYxLjgzNiw5LjkzNiA2MS43MzgsOS42NDcgNjEuNTcyLDkuNDA3IiBpZD0iRmlsbC04MCIgZmlsbD0iIzVCQjlBMSIgc2tldGNoOnR5cGU9Ik1TU2hhcGVHcm91cCI+PC9wYXRoPgogICAgICAgICAgICA8cGF0aCBkPSJNNjAuNjY0LDE2IEw2MC42NjQsNzAuMTQgTDQ3LDcwLjE0IEw0NywxNi4wNjMgTDYwLjY2NCwxNiIgaWQ9IkZpbGwtODEiIGZpbGw9IiM0RDI5NjYiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiPjwvcGF0aD4KICAgICAgICAgICAgPHBhdGggZD0iTTY4LDcwLjE0IEw2OCwxNiBMNzguNTY5LDE2IEw4MC4yNzIsMjIuOTEgQzg2LjgzNywxOC4zMDQgOTMuNDM3LDE2IDEwMC4wNywxNiBDMTEyLjU0OCwxNiAxMTguNzg3LDIyLjcyMSAxMTguNzg3LDM2LjE2MSBMMTE4Ljc4Nyw3MC4xNCBMMTA1LjEyNCw3MC4xNCBMMTA1LjEyNCwzNi4wMDYgQzEwNS4xMjQsMzAuMDI1IDEwMi4xNSwyNy4wMzUgOTYuMjA0LDI3LjAzNSBDOTEuNDI2LDI3LjAzNSA4Ni41NzksMjkuMzIxIDgxLjY2MywzMy44OTIgTDgxLjY2Myw3MC4xNCBMNjgsNzAuMTQiIGlkPSJGaWxsLTgyIiBmaWxsPSIjNEQyOTY2IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj48L3BhdGg+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMzkuNjY0LDU2LjAxMiBDMTQzLjQxMSw1Ny44NjggMTQ3LjU4Nyw1OC43OTcgMTUyLjE5Myw1OC43OTcgQzE2MC42ODMsNTguNzk3IDE2NC45MjksNTMuMDcyIDE2NC45MjksNDEuNjI2IEMxNjQuOTI5LDMxLjg5OCAxNjAuMDgzLDI3LjAzNSAxNTAuMzg4LDI3LjAzNSBDMTQ1LjgxOCwyNy4wMzUgMTQyLjI0MSwyNy4yNzUgMTM5LjY2NCwyNy43NTYgTDEzOS42NjQsNTYuMDEyIEwxMzkuNjY0LDU2LjAxMiBaIE0xMjYsMTguMjE4IEMxMzMuMTg1LDE2Ljc0IDE0MS4xMjQsMTYgMTQ5LjgyMSwxNiBDMTY4LjksMTYgMTc4LjQzOSwyNC41NzcgMTc4LjQzOSw0MS43MjkgQzE3OC40MzksNjAuNjcgMTY5LjcwNiw3MC4xNCAxNTIuMjQ1LDcwLjE0IEMxNDcuOTEzLDcwLjE0IDE0My43Miw2OS4yOCAxMzkuNjY0LDY3LjU2MSBMMTM5LjY2NCw4OS43MzMgTDEyNiw4OS43MzMgTDEyNiwxOC4yMTggTDEyNiwxOC4yMTggWiIgaWQ9IkZpbGwtODMiIGZpbGw9IiM0RDI5NjYiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiPjwvcGF0aD4KICAgICAgICAgICAgPHBhdGggZD0iTTEzLjU2MSwyNi4zNzcgTDEzLjU2MSwxMS43NTYgTDM4LjYzNCwxMS43NTYgTDQ2LjU3MywwIEwwLDAgTDAsNjkuNzkxIEwxMy40MDYsNjkuNzkxIEwxMy40MDYsMzguNTk2IEwzMC4zOCwzOC41OTYgTDM4LjY4NCwyNi4zNzcgTDEzLjU2MSwyNi4zNzciIGlkPSJGaWxsLTg0IiBmaWxsPSIjNEQyOTY2IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj48L3BhdGg+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMTguMDkyLDU1LjA4NCBDMjE0LjIwNyw1Ny45MDMgMjA5LjQ4LDU5LjMxMiAyMDMuOTEyLDU5LjMxMiBDMTk4LjQxNCw1OS4zMTIgMTk1LjY2NCw1Ny4yNDkgMTk1LjY2NCw1My4xMjQgQzE5NS42NjQsNDguOTk5IDE5OC45NDUsNDYuOTM3IDIwNS41MSw0Ni45MzcgQzIxMC40NjEsNDYuOTM3IDIxNC42NTQsNDcuMjgxIDIxOC4wOTIsNDcuOTY4IEwyMTguMDkyLDU1LjA4NCBMMjE4LjA5Miw1NS4wODQgWiBNMjA0LjAxNiwxNiBDMTk4LjEwOSwxNiAxOTIuMjM0LDE2Ljg1OSAxODYuMzk0LDE4LjU3NSBMMTkyLjcwMywyNy44OSBDMTk2LjQ1OSwyNy4xODUgMjAwLjIzLDI2LjgyOCAyMDQuMDE2LDI2LjgyOCBDMjEzLjQsMjYuODI4IDIxOC4wOTIsMjkuMjUyIDIxOC4wOTIsMzQuMDk5IEwyMTguMDkyLDM3LjY1NiBDMjE0LjQxNCwzNi45NjkgMjEwLjIyMSwzNi42MjUgMjA1LjUxLDM2LjYyNSBDMTg5LjgzNiwzNi42MjUgMTgyLDQyLjE3NiAxODIsNTMuMjc5IEMxODIsNjQuNTIgMTg5LjMwMyw3MC4xNCAyMDMuOTEyLDcwLjE0IEMyMDkuMjQsNzAuMTQgMjE0LjI2LDY4Ljg2NyAyMTguOTY5LDY2LjMyNCBMMjI0Ljg0OCw3MC4xNCBMMjMxLjc1NCw3MC4xNCBMMjMxLjc1NCwzMy44NDEgQzIzMS43NTQsMjEuOTQ3IDIyMi41MSwxNiAyMDQuMDE2LDE2IEwyMDQuMDE2LDE2IFoiIGlkPSJGaWxsLTg1IiBmaWxsPSIjNEQyOTY2IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj48L3BhdGg+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4="

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _Feedback = __webpack_require__(76);
  
  var _Feedback2 = _interopRequireDefault(_Feedback);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var Feedback = function (_Component) {
    (0, _inherits3.default)(Feedback, _Component);
  
    function Feedback() {
      (0, _classCallCheck3.default)(this, Feedback);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(Feedback).apply(this, arguments));
    }
  
    (0, _createClass3.default)(Feedback, [{
      key: 'render',
      value: function render() {
        return _react2.default.createElement('div', null);
      }
    }]);
    return Feedback;
  }(_react.Component); /**
                        * React Starter Kit (https://www.reactstarterkit.com/)
                        *
                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                        *
                        * This source code is licensed under the MIT license found in the
                        * LICENSE.txt file in the root directory of this source tree.
                        */
  
  exports.default = (0, _withStyles2.default)(Feedback, _Feedback2.default);

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(77);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, ".LW7n{background:#f5f5f5;color:#333}._3dVJ{margin:0 auto;padding:20px 8px;max-width:1000px;text-align:center;font-size:1.5em}._17lN,._17lN:active,._17lN:hover,._17lN:visited{color:#333;text-decoration:none}._17lN:hover{text-decoration:underline}.Iutw{padding-right:15px;padding-left:15px}", ""]);
  
  // exports
  exports.locals = {
  	"root": "LW7n",
  	"container": "_3dVJ",
  	"link": "_17lN",
  	"spacer": "Iutw"
  };

/***/ },
/* 78 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _Footer = __webpack_require__(79);
  
  var _Footer2 = _interopRequireDefault(_Footer);
  
  var _Link = __webpack_require__(61);
  
  var _Link2 = _interopRequireDefault(_Link);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var Footer = function (_Component) {
    (0, _inherits3.default)(Footer, _Component);
  
    function Footer() {
      (0, _classCallCheck3.default)(this, Footer);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(Footer).apply(this, arguments));
    }
  
    (0, _createClass3.default)(Footer, [{
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: _Footer2.default.root },
          _react2.default.createElement(
            'div',
            { className: _Footer2.default.container },
            _react2.default.createElement(
              'span',
              { className: _Footer2.default.text },
              '© The Finpal'
            ),
            _react2.default.createElement(
              'span',
              { className: _Footer2.default.spacer },
              '·'
            ),
            _react2.default.createElement(
              _Link2.default,
              { className: _Footer2.default.link, to: '/' },
              'Home'
            ),
            _react2.default.createElement(
              'span',
              { className: _Footer2.default.spacer },
              '·'
            ),
            _react2.default.createElement(
              _Link2.default,
              { className: _Footer2.default.link, to: '/privacy' },
              'Privacy'
            ),
            _react2.default.createElement(
              'span',
              { className: _Footer2.default.spacer },
              '·'
            ),
            _react2.default.createElement(
              _Link2.default,
              { className: _Footer2.default.link, to: '/not-found' },
              'Not Found'
            )
          )
        );
      }
    }]);
    return Footer;
  }(_react.Component);
  
  exports.default = (0, _withStyles2.default)(Footer, _Footer2.default);

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(80);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 80 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._3dP9{color:#444;font-size:.85rem}._26pK{*zoom:1;max-width:50em;margin-left:auto;margin-right:auto;text-align:center;padding-top:2rem;padding-bottom:2rem;margin-bottom:1rem}._26pK:after,._26pK:before{content:'';display:table}._26pK:after{clear:both}._1h3n,.tTpl{color:#666}._3n7L{color:#999}.NoJN,.tTpl{padding:2px 5px;font-size:1em}.NoJN,.NoJN:active,.NoJN:visited{color:#666;text-decoration:none}.NoJN:hover{color:#444}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_3dP9",
  	"container": "_26pK",
  	"text": "tTpl",
  	"textMuted": "_1h3n tTpl",
  	"spacer": "_3n7L",
  	"link": "NoJN"
  };

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _HomePage = __webpack_require__(82);
  
  var _HomePage2 = _interopRequireDefault(_HomePage);
  
  var _Slider = __webpack_require__(84);
  
  var _Slider2 = _interopRequireDefault(_Slider);
  
  var _Services = __webpack_require__(92);
  
  var _Services2 = _interopRequireDefault(_Services);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var title = 'The Finpal - Accounting & Financial Services in India'; /**
                                                                        * React Starter Kit (https://www.reactstarterkit.com/)
                                                                        *
                                                                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                                                                        *
                                                                        * This source code is licensed under the MIT license found in the
                                                                        * LICENSE.txt file in the root directory of this source tree.
                                                                        */
  
  var HomePage = function (_Component) {
    (0, _inherits3.default)(HomePage, _Component);
    (0, _createClass3.default)(HomePage, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.context.onSetTitle(title);
      }
    }]);
  
    function HomePage(props) {
      (0, _classCallCheck3.default)(this, HomePage);
  
      var _this = (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(HomePage).call(this, props));
  
      _this.state = {
        visible: false
      };
  
      // start a company
      _this.company = [{ id: 1, name: "Private Limited Company" }, { id: 2, name: "Limited Liability Partnership" }, { id: 3, name: "One Person Company" }, { id: 4, name: "Partnership Firm" }, { id: 5, name: "Proprietorship Firm" }, { id: 6, name: "Public Limited Company" }, { id: 7, name: "Producer Company" }];
  
      // Taxation
      _this.taxtion = [{ id: 11, name: "Service Tax Registration" }, { id: 12, name: "VAT Registration" }, { id: 13, name: "Import Export Code" }, { id: 14, name: "TDS Return" }, { id: 15, name: "VAT Return" }, { id: 16, name: "Service Tax Return" }, { id: 17, name: "Income Tax Return" }, { id: 18, name: "Luxury Tax Return" }];
  
      // Taxation
      _this.audit = [{ id: 21, name: "Private Limited Company" }, { id: 22, name: "Limited Liability Partnership" }, { id: 23, name: "One Person Company" }, { id: 24, name: "Partnership Firm" }, { id: 25, name: "Proprietorship Firm" }, { id: 26, name: "Public Limited Company" }, { id: 27, name: "Producer Company" }, { id: 28, name: "Charitable Trust" }, { id: 29, name: "Society/MSCS/Co-Op Society" }, { id: 30, name: "Educational Institution" }, { id: 31, name: "Chit/Nidhi Company" }];
  
      _this.show = 0;
  
      return _this;
    }
  
    (0, _createClass3.default)(HomePage, [{
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: _HomePage2.default.root },
          _react2.default.createElement(
            'div',
            { className: 'full' },
            _react2.default.createElement(_Slider2.default, null),
            _react2.default.createElement(
              'div',
              { className: (_HomePage2.default.container, _HomePage2.default.container_center) },
              _react2.default.createElement(
                'h3',
                null,
                'Our Services'
              ),
              _react2.default.createElement(
                'p',
                { className: _HomePage2.default.IntroText },
                'Click to on a service to get started'
              )
            ),
            _react2.default.createElement(
              'div',
              { className: _HomePage2.default.container },
              _react2.default.createElement(
                'div',
                { className: _HomePage2.default.ServiceAll, id: 'services' },
                _react2.default.createElement(_Services2.default, { serviceTitle: 'Start a Business', serviceList: this.company, clear: this.show, className: _HomePage2.default.threeCol }),
                _react2.default.createElement(_Services2.default, { serviceTitle: 'Taxation', serviceList: this.taxtion, clear: this.show, className: _HomePage2.default.threeCol }),
                _react2.default.createElement(_Services2.default, { serviceTitle: 'Audit & Assurance', serviceList: this.audit, clear: this.show, className: _HomePage2.default.threeCol })
              )
            )
          )
        );
      }
    }]);
    return HomePage;
  }(_react.Component);
  
  HomePage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(HomePage, _HomePage2.default);

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(83);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "*{box-sizing:border-box}body{margin:0}h3{font-weight:700;font-family:Benton Sans,Helvetica,sans-serif}._1LNa{border-top:1px solid #eee;*zoom:1}._1LNa:after,._1LNa:before{content:'';display:table}._1LNa:after{clear:both}._mDx{font-family:Benton Sans Book,Helvetica,sans-serif;color:#6c4279}._3qPR{*zoom:1;max-width:50em;margin-left:auto;margin-right:auto;text-align:center}._3qPR:after,._3qPR:before{content:'';display:table}._3qPR:after{clear:both}._1JIj{*zoom:1;max-width:50em;margin-left:auto;margin-right:auto}._1JIj:after,._1JIj:before{content:'';display:table}._1JIj:after{clear:both}.OtS0 input,.OtS0 textarea{width:80%!important;padding:.45rem;margin-bottom:1rem;padding-left:.65rem}.Y5W2{width:33.33%;padding:1.3em 0 1.3em 2rem;background-color:#fbfbfb}.Y5W2:nth-child(1n){float:left;margin-right:0;clear:none}.Y5W2:last-child{margin-right:0}.Y5W2:nth-child(3n){float:right;margin-right:0}.Y5W2:nth-child(3n+1){clear:left}.Y5W2 h2{font-family:Benton Sans,Helvetica,sans-serif;padding-bottom:.24em;margin-bottom:0;font-size:1.25em}@media (min-width:24em){.Y5W2{width:99.99%;text-align:center}.Y5W2:nth-child(1n){float:left;margin-right:0;clear:none}.Y5W2:last-child{margin-right:0}.Y5W2:nth-child(1n){float:right;margin-right:0}.Y5W2:nth-child(1n+1){clear:left}}@media (min-width:29.75em){.Y5W2{width:49.995%}.Y5W2:nth-child(1n){float:left;margin-right:0;clear:none}.Y5W2:last-child{margin-right:0}.Y5W2:nth-child(2n){float:right;margin-right:0}.Y5W2:nth-child(2n+1){clear:left}}@media (min-width:50em){.Y5W2{width:33.33%;text-align:left}.Y5W2:nth-child(1n){float:left;margin-right:0;clear:none}.Y5W2:last-child{margin-right:0}.Y5W2:nth-child(3n){float:right;margin-right:0}.Y5W2:nth-child(3n+1){clear:left}}", ""]);
  
  // exports
  exports.locals = {
  	"root": "cbhT",
  	"ServiceAll": "_1LNa",
  	"IntroText": "_mDx",
  	"container_center": "_3qPR",
  	"container": "_1JIj",
  	"form-group": "OtS0",
  	"threeCol": "Y5W2"
  };

/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _classnames = __webpack_require__(71);
  
  var _classnames2 = _interopRequireDefault(_classnames);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _Slider = __webpack_require__(85);
  
  var _Slider2 = _interopRequireDefault(_Slider);
  
  var _Link = __webpack_require__(61);
  
  var _Link2 = _interopRequireDefault(_Link);
  
  var _ModalBody = __webpack_require__(87);
  
  var _ModalBody2 = _interopRequireDefault(_ModalBody);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var Slider = function (_Component) {
    (0, _inherits3.default)(Slider, _Component);
  
    function Slider(props) {
      (0, _classCallCheck3.default)(this, Slider);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(Slider).call(this, props));
    }
  
    (0, _createClass3.default)(Slider, [{
      key: 'render',
      value: function render() {
        var imgUrl = './finance-calicut-accounts-bw.jpg';
  
        var divStyle = {
          backgroundImage: 'url(' + imgUrl + ')',
          WebkitTransition: 'all', // note the capital 'W' here
          msTransition: 'all' // 'ms' is the only lowercase vendor prefix
        };
  
        return _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)(_Slider2.default.root, this.props.className) },
          _react2.default.createElement('div', { style: divStyle, className: _Slider2.default.ImageBg }),
          _react2.default.createElement(
            'div',
            { className: _Slider2.default.taglineHolder },
            _react2.default.createElement(
              'h3',
              { className: _Slider2.default.tagline },
              'your total financial consultant'
            ),
            _react2.default.createElement(
              'a',
              { href: '/#services', className: _Slider2.default.BtnCto },
              'Get Started'
            )
          )
        );
      }
    }]);
    return Slider;
  }(_react.Component);
  
  Slider.propTypes = {
    className: _react.PropTypes.string
  };
  exports.default = (0, _withStyles2.default)(Slider, _Slider2.default);

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(86);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._1r_Q{width:100%;position:relative;text-align:center;font-family:Benton Sans Book,Helvetica,sans-serif}._2_JU{height:90vh;width:100%;margin:0 auto;display:inline-block;box-sizing:border-box;background-repeat:no-repeat;background-size:100%;opacity:.2}._1pH_{position:absolute;bottom:0;height:100%;top:0;left:0;right:0}._3ApJ{border-radius:10rem;background-color:#59c2ae;color:#fff;border:none;padding:.75rem 2rem .65rem;text-decoration:none;font-weight:400!important;font-family:Benton Sans Book,Helvetica,sans-serif}._1xHo{width:100%;position:relative;text-align:center;background:#fff}._2vfF{width:33%;color:#592a7b;font-size:2rem;font-family:Benton Sans,Helvetica,sans-serif;font-weight:700}._1QzC,._2vfF{text-align:center;margin-left:auto;margin-right:auto}._1QzC{position:absolute;bottom:25%;width:100%;z-index:80}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_1r_Q",
  	"ImageBg": "_2_JU",
  	"shader": "_1pH_",
  	"BtnCto": "_3ApJ",
  	"fullSliderImage": "_1xHo",
  	"tagline": "_2vfF",
  	"taglineHolder": "_1QzC"
  };

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _classnames = __webpack_require__(71);
  
  var _classnames2 = _interopRequireDefault(_classnames);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _ModalBody = __webpack_require__(88);
  
  var _ModalBody2 = _interopRequireDefault(_ModalBody);
  
  var _tcombForm = __webpack_require__(90);
  
  var _tcombForm2 = _interopRequireDefault(_tcombForm);
  
  var _reactSkylight = __webpack_require__(91);
  
  var _reactSkylight2 = _interopRequireDefault(_reactSkylight);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var Form = _tcombForm2.default.form.Form;
  
  var Person = _tcombForm2.default.struct({
    name: _tcombForm2.default.String,
    email: _tcombForm2.default.String,
    phone: _tcombForm2.default.Number,
    service: _tcombForm2.default.String,
    message: _tcombForm2.default.String
  });
  
  var options = {
    fields: {
      message: {
        type: 'textarea'
      }
    }
  };
  
  var ModalBody = function (_Component) {
    (0, _inherits3.default)(ModalBody, _Component);
  
    function ModalBody(props) {
      (0, _classCallCheck3.default)(this, ModalBody);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(ModalBody).call(this, props));
    }
  
    (0, _createClass3.default)(ModalBody, [{
      key: 'save',
      value: function save() {
        // call getValue() to get the values of the form
        var value = this.refs.form.getValue();
        // if validation fails, value will be null
        if (value) {
          // value here is an instance of Person
          console.log(value);
        } else {
          console.log('none');
        }
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            _reactSkylight2.default,
            { hideOnOverlayClicked: true, ref: 'simpleDialog', title: this.props.title },
            'Hello, I dont have any callback.'
          )
        );
      }
    }]);
    return ModalBody;
  }(_react.Component);
  
  ModalBody.propTypes = {
    title: _react.PropTypes.string
  };
  exports.default = (0, _withStyles2.default)(ModalBody, _ModalBody2.default);

/***/ },
/* 88 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(89);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 89 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._3Nlm{background:#fff}._3nei{border-radius:10rem;background-color:#59c2ae;color:#fff;border:none;padding:.75rem 2rem .65rem;text-decoration:none}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_3Nlm",
  	"BtnCto": "_3nei"
  };

/***/ },
/* 90 */
/***/ function(module, exports) {

  module.exports = require("tcomb-form");

/***/ },
/* 91 */
/***/ function(module, exports) {

  module.exports = require("react-skylight");

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _classnames = __webpack_require__(71);
  
  var _classnames2 = _interopRequireDefault(_classnames);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _Services = __webpack_require__(93);
  
  var _Services2 = _interopRequireDefault(_Services);
  
  var _ModalForm = __webpack_require__(95);
  
  var _ModalForm2 = _interopRequireDefault(_ModalForm);
  
  var _tcombForm = __webpack_require__(90);
  
  var _tcombForm2 = _interopRequireDefault(_tcombForm);
  
  var _reactSkylight = __webpack_require__(91);
  
  var _reactSkylight2 = _interopRequireDefault(_reactSkylight);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var Services = function (_Component) {
    (0, _inherits3.default)(Services, _Component);
    (0, _createClass3.default)(Services, [{
      key: '_executeBeforeModalClose',
      value: function _executeBeforeModalClose() {
        this.setState({ sent: 0 });
      }
    }]);
  
    function Services(props) {
      (0, _classCallCheck3.default)(this, Services);
  
      var _this = (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(Services).call(this, props));
  
      _this.state = { selectedOne: '', sent: 0 };
      return _this;
    }
  
    (0, _createClass3.default)(Services, [{
      key: 'handleClick',
      value: function handleClick(index) {
        this.setState({ selectedOne: index, sent: 1 }, function () {
          this.refs.simpleDialog.show();
        }.bind(this));
      }
    }, {
      key: 'changeState',
      value: function changeState(e) {}
    }, {
      key: 'render',
      value: function render() {
        var myBigGreenDialog = {
          zIndex: '1200',
          height: '70%'
        };
  
        var servicesList = this.props.serviceList.map(function (service, i) {
          var boundClick = this.handleClick.bind(this, service.name);
  
          return _react2.default.createElement(
            'div',
            { key: service.id, className: _Services2.default.ServiceList },
            _react2.default.createElement(
              'strong',
              { className: _Services2.default.ServiceList___item, onClick: boundClick, key: service.serviceKey },
              service.name
            )
          );
        }, this);
  
        return _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)(_Services2.default.root, this.props.className) },
          _react2.default.createElement(
            'h3',
            { className: _Services2.default.ServiceTitle },
            this.props.serviceTitle
          ),
          _react2.default.createElement(
            _reactSkylight2.default,
            { dialogStyles: myBigGreenDialog, beforeClose: this._executeBeforeModalClose.bind(this), hideOnOverlayClicked: true, hideOnOverlayClicked: true, ref: 'simpleDialog', title: this.props.title },
            _react2.default.createElement(_ModalForm2.default, { serviceHead: this.props.serviceTitle, clearScreen: this.props.clear, selectedOne: this.state.selectedOne })
          ),
          servicesList
        );
      }
    }]);
    return Services;
  }(_react.Component); /**
                        * React Starter Kit (https://www.reactstarterkit.com/)
                        *
                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                        *
                        * This source code is licensed under the MIT license found in the
                        * LICENSE.txt file in the root directory of this source tree.
                        */
  
  Services.propTypes = {
    title: _react.PropTypes.string,
    className: _react.PropTypes.string,
    serviceList: _react.PropTypes.array,
    key: _react.PropTypes.string,
    clear: _react.PropTypes.number,
    serviceTitle: _react.PropTypes.string
  };
  exports.default = (0, _withStyles2.default)(Services, _Services2.default);

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(94);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._26eY{background:#fff}.Oxb8{padding:.35;background-color:#000;color:#fff}._1yli{border:1px solid #eee;padding:14px 0 38px 37px;margin:0;margin-right:-21px;background-color:#fbfbfb}._2Ba4{font-weight:700;font-family:Benton Sans,Helvetica,sans-serif;color:#803aa9}._1zaU,._2Ug5{color:blue}._1zaU,._2Ug5,._2Ug5 h2,.Bfmz{font-family:Benton Sans Book,Helvetica,sans-serif}.Bfmz{background-color:red}.Bfmz fieldset{border:none;padding:0}.Bfmz ._3AAR>input{padding:2em}._1fAT{font-weight:400;font-size:.93rem;color:#616379;border-bottom:1px solid #fff}._1fAT,._1fAT:hover{font-family:Benton Sans Book,Helvetica,sans-serif;display:inline-block;padding-bottom:.3em;margin-bottom:.3em}._1fAT:hover{color:#298c89;border-bottom:1px solid #8962b5;cursor:pointer}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_26eY",
  	"BtnCto": "Oxb8",
  	"ServiceBox": "_1yli",
  	"ServiceTitle": "_2Ba4",
  	"skylight-wrapper": "_1zaU",
  	"modalBox": "_2Ug5",
  	"modelItem": "Bfmz",
  	"form-group": "_3AAR",
  	"ServiceList___item": "_1fAT"
  };

/***/ },
/* 95 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _classnames = __webpack_require__(71);
  
  var _classnames2 = _interopRequireDefault(_classnames);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _ModalForm = __webpack_require__(96);
  
  var _ModalForm2 = _interopRequireDefault(_ModalForm);
  
  var _userActions = __webpack_require__(98);
  
  var _userActions2 = _interopRequireDefault(_userActions);
  
  var _tcombForm = __webpack_require__(90);
  
  var _tcombForm2 = _interopRequireDefault(_tcombForm);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var Form = _tcombForm2.default.form.Form;
  
  // Forms
  
  var Person = _tcombForm2.default.struct({
    name: _tcombForm2.default.String,
    email: _tcombForm2.default.String,
    phone: _tcombForm2.default.Number,
    message: _tcombForm2.default.String
  });
  
  //
  
  var ModalForm = function (_Component) {
    (0, _inherits3.default)(ModalForm, _Component);
  
    function ModalForm(props) {
      (0, _classCallCheck3.default)(this, ModalForm);
  
      var _this = (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(ModalForm).call(this, props));
  
      _this.state = { selectedOne: '', sent: _this.props.clearScreen };
      return _this;
    }
  
    (0, _createClass3.default)(ModalForm, [{
      key: 'shouldComponentUpdate',
      value: function shouldComponentUpdate() {
        return true;
      }
    }, {
      key: 'renderForm',
      value: function renderForm() {
        var stat = this.state.sent;
        if (stat > 0) {
          return _react2.default.createElement(
            'p',
            { className: _ModalForm2.default.SuccessMessage },
            _react2.default.createElement(
              'span',
              null,
              'Your Enquiry has been sent, We will get back to you at the earliest.'
            )
          );
        }
      }
    }, {
      key: 'componentWillReceiveProps',
      value: function componentWillReceiveProps(newProps) {
        this.setState({ sent: newProps.clearScreen });
        console.log(newProps);
      }
    }, {
      key: 'render',
      value: function render() {
        var options = {
          fields: { message: { type: 'textarea', attrs: { rows: 5 } } },
          auto: 'placeholders'
        };
  
        return _react2.default.createElement(
          'div',
          { className: _ModalForm2.default.modelItem },
          _react2.default.createElement(
            'h2',
            null,
            'Send Enquiry'
          ),
          _react2.default.createElement(
            'h5',
            { className: _ModalForm2.default.serviceItem__title },
            this.props.serviceHead,
            ' (',
            this.props.selectedOne,
            ')'
          ),
          this.renderForm(),
          _react2.default.createElement(
            'form',
            { onSubmit: this.sendMessage.bind(this) },
            _react2.default.createElement(Form, { ref: 'form', type: Person, options: options }),
            _react2.default.createElement('input', { type: 'submit', value: 'Send Enquiry', className: _ModalForm2.default.BtnCto })
          )
        );
      }
  
      // Send message via API
  
    }, {
      key: 'sendMessage',
      value: function sendMessage(e) {
        var value = this.refs.form.getValue();
        e.preventDefault();
        if (!value) {
          console.log("Failed.");
        } else {
          var serviceSelected = this.props.serviceHead + '( ' + this.props.selectedOne + ')';
          _userActions2.default.message(value, serviceSelected);
          //
          this.setState({ sent: 1 }, function () {
            console.log('done?');
          }.bind(this));
        }
      }
    }]);
    return ModalForm;
  }(_react.Component);
  
  ModalForm.propTypes = {
    title: _react.PropTypes.string,
    selectedOne: _react.PropTypes.string,
    sent: _react.PropTypes.number,
    clearScreen: _react.PropTypes.number,
    serviceHead: _react.PropTypes.string,
    parentHead: _react.PropTypes.string
  };
  exports.default = (0, _withStyles2.default)(ModalForm, _ModalForm2.default);

/***/ },
/* 96 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(97);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 97 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._2HxY{background:#fff;font-family:Benton Sans Book,Helvetica,sans-serif}.bMaY{border-radius:10rem;background-color:#59c2ae;color:#fff;border:none;padding:.75rem 2rem .65rem;text-decoration:none;font-family:Benton Sans,Helvetica,sans-serif;font-size:.9em;cursor:pointer}._3r7e{font-family:Benton Sans Book,Helvetica,sans-serif;margin:0;margin-bottom:1em}input[type=text]{padding:.5em;padding-left:.75em;padding-right:.65em}input[type=text],textarea{border:1px solid #000;font-family:Benton Sans Book,Helvetica,sans-serif;font-size:1em;width:90%;margin-bottom:.5em}textarea{padding-left:.75em;padding-top:1em;padding-right:.65em}._1SPA{font-family:Benton Sans Book,Helvetica,sans-serif;padding-left:3em}._1SPA fieldset{border:none;padding:0;padding-top:1.025em}._1SPA ._29PQ input{background-color:blue}._1SPA ._2EhB{width:100%;background-color:#ff0;margin-bottom:1em}.npQH{padding:1rem 1.4em;background-color:#c6e8e8;color:#206c71;margin-right:3.7em}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_2HxY",
  	"BtnCto": "bMaY",
  	"serviceItem__title": "_3r7e",
  	"modelItem": "_1SPA",
  	"form-group": "_29PQ",
  	"form-control": "_2EhB",
  	"SuccessMessage": "npQH"
  };

/***/ },
/* 98 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _Dispatcher = __webpack_require__(99);
  
  var _Dispatcher2 = _interopRequireDefault(_Dispatcher);
  
  var _ActionTypes = __webpack_require__(101);
  
  var _ActionTypes2 = _interopRequireDefault(_ActionTypes);
  
  var _AuthConstants = __webpack_require__(103);
  
  var _Location = __webpack_require__(65);
  
  var _Location2 = _interopRequireDefault(_Location);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  //
  
  exports.default = {
    message: function message(form, service) {
      if (!form) {
        console.log("Failed.");
      } else {
        _Dispatcher2.default.dispatch({
          actionType: _AuthConstants.SEND_MESSAGE,
          form: form,
          service: service
        });
  
        console.log("Succeeded.");
      }
    },
    login: function login(jwt) {
      var savedJwt = localStorage.getItem('jwt');
  
      _Dispatcher2.default.dispatch({
        actionType: _AuthConstants.LOGIN_USER,
        jwt: jwt
      });
  
      if (savedJwt !== jwt) {
        _Location2.default.push({ pathname: '/dashboard' });
        localStorage.setItem('jwt', jwt);
      }
    },
  
    logoutUser: function logoutUser() {
      _Location2.default.push({ pathname: '/login' });
  
      localStorage.removeItem('jwt');
      _Dispatcher2.default.dispatch({
        actionType: _AuthConstants.LOGOUT_USER
      });
    }
  
  };

/***/ },
/* 99 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _flux = __webpack_require__(100);
  
  exports.default = new _flux.Dispatcher(); /**
                                             * React Starter Kit (https://www.reactstarterkit.com/)
                                             *
                                             * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                                             *
                                             * This source code is licensed under the MIT license found in the
                                             * LICENSE.txt file in the root directory of this source tree.
                                             */

/***/ },
/* 100 */
/***/ function(module, exports) {

  module.exports = require("flux");

/***/ },
/* 101 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _keyMirror = __webpack_require__(102);
  
  var _keyMirror2 = _interopRequireDefault(_keyMirror);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var BASE_URL = 'http://localhost:3000/'; /**
                                            * React Starter Kit (https://www.reactstarterkit.com/)
                                            *
                                            * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                                            *
                                            * This source code is licensed under the MIT license found in the
                                            * LICENSE.txt file in the root directory of this source tree.
                                            */
  
  exports.default = (0, _keyMirror2.default)({
    REGISTER_USER_SUCCESS: null,
    REGISTER_USER_FAIL: null,
    LOGIN_SUCCESS: null,
    LOGIN_FAIL: null,
    BASE_URL: BASE_URL,
    LOGIN_URL: BASE_URL + 'api/session/login',
    SIGNUP_URL: BASE_URL + '/register',
    LOGIN_USER: 'LOGIN_USER',
    LOGOUT_USER: 'LOGOUT_USER'
  });

/***/ },
/* 102 */
/***/ function(module, exports) {

  module.exports = require("fbjs/lib/keyMirror");

/***/ },
/* 103 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _keyMirror = __webpack_require__(102);
  
  var _keyMirror2 = _interopRequireDefault(_keyMirror);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  exports.default = (0, _keyMirror2.default)({
    LOGIN_USER: null,
    LOGOUT_USER: null,
    FAILED_LOGIN: null,
    UNAUTHORIZED_USER: null,
    SEND_MESSAGE: null,
    SENT_MESSAGE: 0
  });

/***/ },
/* 104 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _ContentPage = __webpack_require__(105);
  
  var _ContentPage2 = _interopRequireDefault(_ContentPage);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var ContentPage = function (_Component) {
    (0, _inherits3.default)(ContentPage, _Component);
  
    function ContentPage() {
      (0, _classCallCheck3.default)(this, ContentPage);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(ContentPage).apply(this, arguments));
    }
  
    (0, _createClass3.default)(ContentPage, [{
      key: 'render',
      value: function render() {
        this.context.onSetTitle(this.props.title);
        return _react2.default.createElement(
          'div',
          { className: _ContentPage2.default.root },
          _react2.default.createElement(
            'div',
            { className: _ContentPage2.default.container },
            this.props.path === '/' ? null : _react2.default.createElement(
              'h1',
              null,
              this.props.title
            ),
            _react2.default.createElement('div', { dangerouslySetInnerHTML: { __html: this.props.content || '' } })
          )
        );
      }
    }]);
    return ContentPage;
  }(_react.Component); /**
                        * React Starter Kit (https://www.reactstarterkit.com/)
                        *
                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                        *
                        * This source code is licensed under the MIT license found in the
                        * LICENSE.txt file in the root directory of this source tree.
                        */
  
  ContentPage.propTypes = {
    path: _react.PropTypes.string.isRequired,
    content: _react.PropTypes.string.isRequired,
    title: _react.PropTypes.string
  };
  ContentPage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(ContentPage, _ContentPage2.default);

/***/ },
/* 105 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(106);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 106 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._1JTr{margin:0 auto;padding:0 0 40px;max-width:1000px}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_1Kg7",
  	"container": "_1JTr"
  };

/***/ },
/* 107 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _ContactPage = __webpack_require__(108);
  
  var _ContactPage2 = _interopRequireDefault(_ContactPage);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var title = 'Contact Us'; /**
                             * React Starter Kit (https://www.reactstarterkit.com/)
                             *
                             * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                             *
                             * This source code is licensed under the MIT license found in the
                             * LICENSE.txt file in the root directory of this source tree.
                             */
  
  var ContactPage = function (_Component) {
    (0, _inherits3.default)(ContactPage, _Component);
  
    function ContactPage() {
      (0, _classCallCheck3.default)(this, ContactPage);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(ContactPage).apply(this, arguments));
    }
  
    (0, _createClass3.default)(ContactPage, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.context.onSetTitle(title);
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: _ContactPage2.default.root },
          _react2.default.createElement(
            'div',
            { className: _ContactPage2.default.container },
            _react2.default.createElement(
              'h1',
              null,
              title
            ),
            _react2.default.createElement(
              'p',
              null,
              '...'
            )
          )
        );
      }
    }]);
    return ContactPage;
  }(_react.Component);
  
  ContactPage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(ContactPage, _ContactPage2.default);

/***/ },
/* 108 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(109);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 109 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._2pQ3{margin:0 auto;padding:0 0 40px;max-width:1000px}", ""]);
  
  // exports
  exports.locals = {
  	"root": "c4zC",
  	"container": "_2pQ3"
  };

/***/ },
/* 110 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _AboutPage = __webpack_require__(111);
  
  var _AboutPage2 = _interopRequireDefault(_AboutPage);
  
  var _Slider = __webpack_require__(84);
  
  var _Slider2 = _interopRequireDefault(_Slider);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var title = 'About us';
  
  var AboutPage = function (_Component) {
    (0, _inherits3.default)(AboutPage, _Component);
  
    function AboutPage() {
      (0, _classCallCheck3.default)(this, AboutPage);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(AboutPage).apply(this, arguments));
    }
  
    (0, _createClass3.default)(AboutPage, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.context.onSetTitle(title);
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: _AboutPage2.default.root },
          _react2.default.createElement(_Slider2.default, null),
          _react2.default.createElement(
            'div',
            { className: _AboutPage2.default.container },
            _react2.default.createElement(
              'h1',
              null,
              title
            ),
            _react2.default.createElement(
              'p',
              null,
              'The Finpal is a team of Financial consultants based out Calicut, India'
            )
          )
        );
      }
    }]);
    return AboutPage;
  }(_react.Component);
  
  AboutPage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(AboutPage, _AboutPage2.default);

/***/ },
/* 111 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(112);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 112 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._2_TE,body{font-family:Benton Sans Book,sans-serif}body{margin:0}._22I6{margin:0 auto;padding:0 0 40px;max-width:1000px;font-size:1.1em}._3apY,._22I6{line-height:1.7;color:#6b7475}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_2_TE",
  	"container": "_22I6",
  	"about": "_3apY"
  };

/***/ },
/* 113 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _LoginPage = __webpack_require__(114);
  
  var _LoginPage2 = _interopRequireDefault(_LoginPage);
  
  var _InputField = __webpack_require__(116);
  
  var _InputField2 = _interopRequireDefault(_InputField);
  
  var _FormButton = __webpack_require__(119);
  
  var _FormButton2 = _interopRequireDefault(_FormButton);
  
  var _userActions = __webpack_require__(98);
  
  var _userActions2 = _interopRequireDefault(_userActions);
  
  var _userStore = __webpack_require__(122);
  
  var _userStore2 = _interopRequireDefault(_userStore);
  
  var _Slider = __webpack_require__(84);
  
  var _Slider2 = _interopRequireDefault(_Slider);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var title = 'Log In';
  
  // const title = 'Log In';
  
  var LoginPage = function (_Component) {
    (0, _inherits3.default)(LoginPage, _Component);
  
    function LoginPage(props) {
      (0, _classCallCheck3.default)(this, LoginPage);
  
      var _this = (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(LoginPage).call(this, props));
  
      _this.state = {
        username: '',
        password: ''
      };
      _this.onChange = _this.onChange.bind(_this);
      _this.readyToSubmit = _this.readyToSubmit.bind(_this);
      _this.submit = _this.submit.bind(_this);
      return _this;
    }
  
    (0, _createClass3.default)(LoginPage, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        var _this2 = this;
  
        this.context.onSetTitle(title);
        _userStore2.default.addChangeListener(function () {
          _this2.handleUserState();
        });
      }
    }, {
      key: 'onChange',
      value: function onChange(e) {
        switch (e.target.id) {
          case 'username':
            this.setState({
              username: e.target.value
            });
            break;
          case 'password':
            this.setState({
              password: e.target.value
            });
            break;
          default:
        }
      }
    }, {
      key: 'handleUserState',
      value: function handleUserState() {
        if (_userStore2.default.getState().success) {
          // window.location.replace('/');
        }
      }
    }, {
      key: 'submit',
      value: function submit() {
        _userActions2.default.login(this.state);
      }
    }, {
      key: 'usernameValid',
      value: function usernameValid() {
        var name = this.state.username;
        return name.length > 3 && name === name.replace(/ /g, '');
      }
    }, {
      key: 'passwordValid',
      value: function passwordValid() {
        // one digit one char of each case at least 8 long
        return (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(this.state.password)
        );
      }
    }, {
      key: 'readyToSubmit',
      value: function readyToSubmit() {
        return this.usernameValid() && this.passwordValid();
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: _LoginPage2.default.root },
          _react2.default.createElement(
            'div',
            { className: 'full' },
            _react2.default.createElement(_Slider2.default, null)
          ),
          _react2.default.createElement(
            'div',
            { className: _LoginPage2.default.container },
            _react2.default.createElement(_InputField2.default, {
              value: this.state.username,
              onChange: this.onChange,
              synValid: this.usernameValid(),
              hideTyping: false,
              id: 'username',
              title: 'Username' }),
            _react2.default.createElement(_InputField2.default, {
              value: this.state.password,
              onChange: this.onChange,
              synValid: this.passwordValid(),
              hideTyping: true,
              id: 'password',
              title: 'Password' }),
            _react2.default.createElement(_FormButton2.default, {
              active: this.readyToSubmit(),
              onClick: this.submit })
          )
        );
      }
    }]);
    return LoginPage;
  }(_react.Component);
  
  LoginPage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(LoginPage, _LoginPage2.default);

/***/ },
/* 114 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(115);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 115 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._2c5c{margin:0 auto;padding:0 0 40px;max-width:1000px}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_5f7Y",
  	"container": "_2c5c"
  };

/***/ },
/* 116 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _InputField = __webpack_require__(117);
  
  var _InputField2 = _interopRequireDefault(_InputField);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var InputField = function (_Component) {
    (0, _inherits3.default)(InputField, _Component);
  
    function InputField(props) {
      (0, _classCallCheck3.default)(this, InputField);
  
      var _this = (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(InputField).call(this, props));
  
      _this.state = {
        active: false
      };
      return _this;
    }
  
    (0, _createClass3.default)(InputField, [{
      key: 'render',
      value: function render() {
        var classes = '';
        if (this.props.synValid) {
          classes += _InputField2.default.synValid;
        }
        if (this.props.value.length > 0) {
          classes += ' ' + _InputField2.default.dirty;
        }
  
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement('input', {
            type: this.props.hideTyping ? "password" : "text",
            value: this.props.value,
            id: this.props.id,
            className: classes,
            onChange: this.props.onChange,
            placeholder: this.props.title
          }),
          this.props.tips ? _react2.default.createElement(
            'p',
            { className: _InputField2.default.help },
            this.props.tips
          ) : null
        );
      }
    }]);
    return InputField;
  }(_react.Component);
  
  InputField.propTypes = {
    title: _react.PropTypes.string.isRequired,
    value: _react.PropTypes.string.isRequired,
    id: _react.PropTypes.string.isRequired,
    hideTyping: _react.PropTypes.bool.isRequired,
    tips: _react.PropTypes.string,
    synValid: _react.PropTypes.bool,
    onChange: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(InputField, _InputField2.default);

/***/ },
/* 117 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(118);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 118 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "input{margin:30px 0 5px;padding:0 0 5px;width:300px;font-size:1.5em;text-align:center;border:none;border-bottom:2.5px solid #f0f0f0;outline:none;background-color:transparent;-webkit-transition:border-bottom .2s ease;transition:border-bottom .2s ease}input:focus{border-bottom-color:$palette-peach}._3WKW,input:focus{-webkit-transition:border-bottom 1s ease;transition:border-bottom 1s ease}._3WKW{border-bottom-color:$palette-red}._3WKW._3g7D{border-bottom-color:$palette-teal}._2ukm{display:block;width:inherit;margin:5px 0;font-size:.7em;color:#787878;text-align:center}", ""]);
  
  // exports
  exports.locals = {
  	"dirty": "_3WKW",
  	"synValid": "_3g7D",
  	"help": "_2ukm"
  };

/***/ },
/* 119 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _FormButton = __webpack_require__(120);
  
  var _FormButton2 = _interopRequireDefault(_FormButton);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var FormButton = function (_Component) {
    (0, _inherits3.default)(FormButton, _Component);
  
    function FormButton(props) {
      (0, _classCallCheck3.default)(this, FormButton);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(FormButton).call(this, props));
    }
  
    (0, _createClass3.default)(FormButton, [{
      key: 'render',
      value: function render() {
        var enabled = this.props.active ? '' : 'disabled';
        var classes = this.props.active ? _FormButton2.default.button + ' ' + _FormButton2.default.ready : _FormButton2.default.button;
  
        return _react2.default.createElement(
          'button',
          {
            className: classes,
            onClick: this.props.onClick
          },
          'Save'
        );
      }
    }]);
    return FormButton;
  }(_react.Component);
  
  FormButton.propTypes = {
    active: _react.PropTypes.bool.isRequired,
    onClick: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(FormButton, _FormButton2.default);

/***/ },
/* 120 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(121);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 121 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._2aNu{margin-top:45px;width:62px;height:60px;outline:none;background-color:#fff;color:#787878;border:1px solid #575757;-webkit-transition:background-color 1s ease,color 1s ease;transition:background-color 1s ease,color 1s ease}._2aNu._2jQ3{color:#000;background-color:$palette-teal}", ""]);
  
  // exports
  exports.locals = {
  	"button": "_2aNu",
  	"ready": "_2jQ3"
  };

/***/ },
/* 122 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _stringify = __webpack_require__(53);
  
  var _stringify2 = _interopRequireDefault(_stringify);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _Dispatcher = __webpack_require__(99);
  
  var _Dispatcher2 = _interopRequireDefault(_Dispatcher);
  
  var _ActionTypes = __webpack_require__(101);
  
  var _ActionTypes2 = _interopRequireDefault(_ActionTypes);
  
  var _events = __webpack_require__(123);
  
  var _objectAssign = __webpack_require__(124);
  
  var _objectAssign2 = _interopRequireDefault(_objectAssign);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var CHANGE_EVENT = 'change';
  
  var _message = {};
  
  var _userState = {
    'action': null,
    'success': null
  };
  
  var userStore = (0, _objectAssign2.default)({}, _events.EventEmitter.prototype, {
    addChangeListener: function addChangeListener(callback) {
      this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener: function removeChangeListener(callback) {
      this.removeListener(CHANGE_EVENT, callback);
    },
    emitChange: function emitChange() {
      this.emit(CHANGE_EVENT);
    },
    getState: function getState() {
      return _userState;
    }
  });
  
  _Dispatcher2.default.register(function (action) {
    switch (action.actionType) {
  
      case _ActionTypes2.default.SEND_MESSAGE:
        send_message(action.form, action.service);
        _userState.action = 'message';
        _userState.success = true;
        userStore.emitChange();
        break;
  
      case _ActionTypes2.default.REGISTER_USER_SUCCESS:
        _userState.action = 'register';
        _userState.success = true;
        userStore.emitChange();
        break;
  
      case _ActionTypes2.default.REGISTER_USER_FAIL:
        _userState.action = 'register';
        _userState.success = false;
        userStore.emitChange();
        break;
  
      case _ActionTypes2.default.LOGIN_SUCCESS:
        _userState.action = 'login';
        _userState.success = true;
        userStore.emitChange();
        break;
  
      case _ActionTypes2.default.LOGIN_FAIL:
        _userState.action = 'login';
        _userState.success = false;
        userStore.emitChange();
        break;
  
      default:
  
    }
  });
  
  /**
   * Send Message.
   * @param  {object} form data
   * @param  {string} selected service
   */
  function send_message(form, service) {
    _message = {
      name: form.name,
      email: form.email,
      phone: form.phone,
      message: form.message,
      service: service
    };
  
    // Send Message via api/v2
    fetch('/api/v2/send', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: (0, _stringify2.default)(_message)
    });
  }
  
  exports.default = userStore;

/***/ },
/* 123 */
/***/ function(module, exports) {

  module.exports = require("events");

/***/ },
/* 124 */
/***/ function(module, exports) {

  module.exports = require("object-assign");

/***/ },
/* 125 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _RegisterPage = __webpack_require__(126);
  
  var _RegisterPage2 = _interopRequireDefault(_RegisterPage);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var title = 'New User Registration'; /**
                                        * React Starter Kit (https://www.reactstarterkit.com/)
                                        *
                                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                                        *
                                        * This source code is licensed under the MIT license found in the
                                        * LICENSE.txt file in the root directory of this source tree.
                                        */
  
  var RegisterPage = function (_Component) {
    (0, _inherits3.default)(RegisterPage, _Component);
  
    function RegisterPage() {
      (0, _classCallCheck3.default)(this, RegisterPage);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(RegisterPage).apply(this, arguments));
    }
  
    (0, _createClass3.default)(RegisterPage, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.context.onSetTitle(title);
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          { className: _RegisterPage2.default.root },
          _react2.default.createElement(
            'div',
            { className: _RegisterPage2.default.container },
            _react2.default.createElement(
              'h1',
              null,
              title
            ),
            _react2.default.createElement(
              'p',
              null,
              '...'
            )
          )
        );
      }
    }]);
    return RegisterPage;
  }(_react.Component);
  
  RegisterPage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(RegisterPage, _RegisterPage2.default);

/***/ },
/* 126 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(127);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 127 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "._6L5l{margin:0 auto;padding:0 0 40px;max-width:1000px}", ""]);
  
  // exports
  exports.locals = {
  	"root": "_2YrP",
  	"container": "_6L5l"
  };

/***/ },
/* 128 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _NotFoundPage = __webpack_require__(129);
  
  var _NotFoundPage2 = _interopRequireDefault(_NotFoundPage);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var title = 'Page Not Found'; /**
                                 * React Starter Kit (https://www.reactstarterkit.com/)
                                 *
                                 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                                 *
                                 * This source code is licensed under the MIT license found in the
                                 * LICENSE.txt file in the root directory of this source tree.
                                 */
  
  var NotFoundPage = function (_Component) {
    (0, _inherits3.default)(NotFoundPage, _Component);
  
    function NotFoundPage() {
      (0, _classCallCheck3.default)(this, NotFoundPage);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(NotFoundPage).apply(this, arguments));
    }
  
    (0, _createClass3.default)(NotFoundPage, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.context.onSetTitle(title);
        this.context.onPageNotFound();
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'h1',
            null,
            title
          ),
          _react2.default.createElement(
            'p',
            null,
            'Sorry, but the page you were trying to view does not exist.'
          )
        );
      }
    }]);
    return NotFoundPage;
  }(_react.Component);
  
  NotFoundPage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired,
    onPageNotFound: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(NotFoundPage, _NotFoundPage2.default);

/***/ },
/* 129 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(130);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 130 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "*{margin:0;line-height:1.2}html{display:table;width:100%;height:100%;color:#888;text-align:center;font-family:sans-serif}body{display:table-cell;margin:2em auto;vertical-align:middle}h1{color:#555;font-weight:400;font-size:2em}p{margin:0 auto;width:280px}@media only screen and (max-width:280px){body,p{width:95%}h1{font-size:1.5em;margin:0 0 .3em}}", ""]);
  
  // exports


/***/ },
/* 131 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _withStyles = __webpack_require__(58);
  
  var _withStyles2 = _interopRequireDefault(_withStyles);
  
  var _ErrorPage = __webpack_require__(132);
  
  var _ErrorPage2 = _interopRequireDefault(_ErrorPage);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var title = 'Error'; /**
                        * React Starter Kit (https://www.reactstarterkit.com/)
                        *
                        * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                        *
                        * This source code is licensed under the MIT license found in the
                        * LICENSE.txt file in the root directory of this source tree.
                        */
  
  var ErrorPage = function (_Component) {
    (0, _inherits3.default)(ErrorPage, _Component);
  
    function ErrorPage() {
      (0, _classCallCheck3.default)(this, ErrorPage);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(ErrorPage).apply(this, arguments));
    }
  
    (0, _createClass3.default)(ErrorPage, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.context.onSetTitle(title);
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'h1',
            null,
            title
          ),
          _react2.default.createElement(
            'p',
            null,
            'Sorry, an critical error occurred on this page.'
          )
        );
      }
    }]);
    return ErrorPage;
  }(_react.Component);
  
  ErrorPage.contextTypes = {
    onSetTitle: _react.PropTypes.func.isRequired,
    onPageNotFound: _react.PropTypes.func.isRequired
  };
  exports.default = (0, _withStyles2.default)(ErrorPage, _ErrorPage2.default);

/***/ },
/* 132 */
/***/ function(module, exports, __webpack_require__) {

  
      var content = __webpack_require__(133);
      var insertCss = __webpack_require__(52);
  
      if (typeof content === 'string') {
        content = [[module.id, content, '']];
      }
  
      module.exports = content.locals || {};
      module.exports._getCss = function() { return content.toString(); };
      module.exports._insertCss = insertCss.bind(null, content);
    

/***/ },
/* 133 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(51)();
  // imports
  
  
  // module
  exports.push([module.id, "*{margin:0;line-height:1.2}html{display:table;width:100%;height:100%;color:#888;text-align:center;font-family:sans-serif}body{display:table-cell;margin:2em auto;vertical-align:middle}h1{color:#555;font-weight:400;font-size:2em}p{margin:0 auto;width:280px}@media only screen and (max-width:280px){body,p{width:95%}h1{font-size:1.5em;margin:0 0 .3em}}", ""]);
  
  // exports


/***/ },
/* 134 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _getPrototypeOf = __webpack_require__(43);
  
  var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  var _possibleConstructorReturn2 = __webpack_require__(46);
  
  var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);
  
  var _inherits2 = __webpack_require__(47);
  
  var _inherits3 = _interopRequireDefault(_inherits2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _react = __webpack_require__(6);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _config = __webpack_require__(41);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * React Starter Kit (https://www.reactstarterkit.com/)
   *
   * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE.txt file in the root directory of this source tree.
   */
  
  var Html = function (_Component) {
    (0, _inherits3.default)(Html, _Component);
  
    function Html() {
      (0, _classCallCheck3.default)(this, Html);
      return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(Html).apply(this, arguments));
    }
  
    (0, _createClass3.default)(Html, [{
      key: 'trackingCode',
      value: function trackingCode() {
        return { __html: '(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=' + 'function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;' + 'e=o.createElement(i);r=o.getElementsByTagName(i)[0];' + 'e.src=\'https://www.google-analytics.com/analytics.js\';' + 'r.parentNode.insertBefore(e,r)}(window,document,\'script\',\'ga\'));' + ('ga(\'create\',\'' + _config.googleAnalyticsId + '\',\'auto\');ga(\'send\',\'pageview\');')
        };
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          'html',
          { className: 'no-js', lang: '' },
          _react2.default.createElement(
            'head',
            null,
            _react2.default.createElement('meta', { charSet: 'utf-8' }),
            _react2.default.createElement('meta', { httpEquiv: 'X-UA-Compatible', content: 'IE=edge' }),
            _react2.default.createElement(
              'title',
              null,
              this.props.title
            ),
            _react2.default.createElement('meta', { name: 'description', content: this.props.description }),
            _react2.default.createElement('meta', { name: 'viewport', content: 'width=device-width, initial-scale=1' }),
            _react2.default.createElement('link', { rel: 'apple-touch-icon', href: 'apple-touch-icon.png' }),
            _react2.default.createElement('style', { id: 'css', dangerouslySetInnerHTML: { __html: this.props.css } })
          ),
          _react2.default.createElement(
            'body',
            null,
            _react2.default.createElement('div', { id: 'app', dangerouslySetInnerHTML: { __html: this.props.body } }),
            _react2.default.createElement('script', { src: this.props.entry }),
            _react2.default.createElement('script', { dangerouslySetInnerHTML: this.trackingCode() })
          )
        );
      }
    }]);
    return Html;
  }(_react.Component);
  
  Html.propTypes = {
    title: _react.PropTypes.string,
    description: _react.PropTypes.string,
    css: _react.PropTypes.string,
    body: _react.PropTypes.string.isRequired,
    entry: _react.PropTypes.string.isRequired
  };
  Html.defaultProps = {
    title: '',
    description: ''
  };
  exports.default = Html;

/***/ },
/* 135 */
/***/ function(module, exports) {

  module.exports = require("./assets");

/***/ },
/* 136 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _express = __webpack_require__(5);
  
  var _MessageModel = __webpack_require__(137);
  
  var _MessageModel2 = _interopRequireDefault(_MessageModel);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var router = new _express.Router();
  
  // Emailers
  var nodemailer = __webpack_require__(139);
  var mandrillTransport = __webpack_require__(140);
  
  // Mandrill Transport
  var transport = nodemailer.createTransport(mandrillTransport({
    auth: {
      apiKey: 'N15NWxI69Dc4LtsrOHWFXQ'
    }
  }));
  
  function sendResponse(res, results) {
    res.status(200);
    console.log(results);
    res.json({ done: 'true' });
  }
  
  // TODO Add error codes when invalid data is supplied
  router.post('/send', function (req, res) {
  
    var message = {
      name: req.body.name,
      email: req.body.email,
      message: req.body.message,
      phone: req.body.phone,
      service: req.body.service,
      publishedAt: new Date()
    };
  
    var messageEntry = new _MessageModel2.default(message);
  
    messageEntry.save(function (err) {
      if (!err) {
        res.json({ status: 'sucess', message: messageEntry });
  
        //
        var html = message.name + " has sent a new enquiry regarding <strong>" + message.service + "</strong>, <br/> " + '<quoteblock style="border: solid 1px #999; padding: 2em; background-color: #efefef;">' + message.message + "</quoteblock> <br/><br/>" + message.name + "<br/>" + message.phone + "<br/>";
  
        // Send Email
        transport.sendMail({
          from: 'Finpal Website 👥 <finpal@aurut.com>', // sender address
          to: 'hey@muneef.in',
          subject: '[Finpal] New enquiry from ' + message.name,
          html: html
        }, function (err, info) {
          if (err) {
            console.error(err);
          } else {
            console.log(info);
          }
        });
      } else {
        console.log(err);
        res.json({ status: 'failed' });
      }
    });
  });
  
  // router.get('/all', (req, res) => {
  //   Message.find({}, function(err, posts){
  //     res.json(posts);
  //   });
  // });
  
  exports.default = router;

/***/ },
/* 137 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var mongoose = __webpack_require__(138);
  var Schema = mongoose.Schema;
  
  // set up a mongoose model and pass it using module.exports
  module.exports = mongoose.model('Message', new Schema({
    name: String,
    email: String,
    message: String,
    service: String,
    publishedAt: Date
  }));

/***/ },
/* 138 */
/***/ function(module, exports) {

  module.exports = require("mongoose");

/***/ },
/* 139 */
/***/ function(module, exports) {

  module.exports = require("nodemailer");

/***/ },
/* 140 */
/***/ function(module, exports) {

  module.exports = require("nodemailer-mandrill-transport");

/***/ },
/* 141 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.IsAuth = exports.init = undefined;
  
  var _passport = __webpack_require__(142);
  
  var _passport2 = _interopRequireDefault(_passport);
  
  var _jsonwebtoken = __webpack_require__(143);
  
  var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);
  
  var _account = __webpack_require__(144);
  
  var _account2 = _interopRequireDefault(_account);
  
  var _passportLocal = __webpack_require__(146);
  
  var _passportJwt = __webpack_require__(147);
  
  var _AuthService = __webpack_require__(148);
  
  var _AuthService2 = _interopRequireDefault(_AuthService);
  
  var _config = __webpack_require__(41);
  
  var _config2 = _interopRequireDefault(_config);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  _passport2.default.use(new _passportLocal.Strategy({
      session: false
  }, function (username, password, cb) {
      //TODO: restore user from DB
      return cb(null, { id: 'asdf234' });
  }));
  
  _passport2.default.use(new _passportJwt.Strategy({
      //TODO: take secret from config
      secretOrKey: 'asdfaaa',
      session: false
  }, function (jwt_payload, cb) {
      //TODO: restore user from DB
      cb(null, { id: 'asdf234' });
  }));
  
  _passport2.default.serializeUser(function (user, cb) {
      cb(null, user.id);
  });
  
  _passport2.default.deserializeUser(function (id, cb) {
      cb(null, { id: id });
  });
  
  var init = exports.init = function init(app) {
      app.use(_passport2.default.initialize());
      authRoutes(app);
  };
  
  var IsAuth = exports.IsAuth = function IsAuth(req, res, next) {
      _passport2.default.authenticate('jwt')(req, res, next);
  };

/***/ },
/* 142 */
/***/ function(module, exports) {

  module.exports = require("passport");

/***/ },
/* 143 */
/***/ function(module, exports) {

  module.exports = require("jsonwebtoken");

/***/ },
/* 144 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _mongoose = __webpack_require__(138);
  
  var _mongoose2 = _interopRequireDefault(_mongoose);
  
  var _passportLocalMongoose = __webpack_require__(145);
  
  var _passportLocalMongoose2 = _interopRequireDefault(_passportLocalMongoose);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var Schema = _mongoose2.default.Schema;
  
  var Account = new Schema({
    username: String
  });
  
  Account.plugin(_passportLocalMongoose2.default);
  
  exports.default = _mongoose2.default.model('Account', Account);

/***/ },
/* 145 */
/***/ function(module, exports) {

  module.exports = require("passport-local-mongoose");

/***/ },
/* 146 */
/***/ function(module, exports) {

  module.exports = require("passport-local");

/***/ },
/* 147 */
/***/ function(module, exports) {

  module.exports = require("passport-jwt");

/***/ },
/* 148 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  var _stringify = __webpack_require__(53);
  
  var _stringify2 = _interopRequireDefault(_stringify);
  
  var _classCallCheck2 = __webpack_require__(44);
  
  var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);
  
  var _createClass2 = __webpack_require__(45);
  
  var _createClass3 = _interopRequireDefault(_createClass2);
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _AuthConstants = __webpack_require__(103);
  
  var _userActions = __webpack_require__(98);
  
  var _userActions2 = _interopRequireDefault(_userActions);
  
  var _bluebird = __webpack_require__(149);
  
  var _bluebird2 = _interopRequireDefault(_bluebird);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var AuthService = function () {
    function AuthService() {
      (0, _classCallCheck3.default)(this, AuthService);
    }
  
    (0, _createClass3.default)(AuthService, [{
      key: 'login',
      value: function login(username, password) {
        return fetch('/api/sessions/login', {
          method: 'post',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: (0, _stringify2.default)({
            username: username,
            password: password
          })
        });
      }
    }, {
      key: 'logout',
      value: function logout() {
        _userActions2.default.logoutUser();
      }
    }, {
      key: 'handleAuth',
      value: function handleAuth(loginPromise) {
        return loginPromise.then(function (response) {
          var jwt = response.id_token;
          _userActions2.default.loginUser(jwt);
          return true;
        });
      }
    }]);
    return AuthService;
  }(); // import request from 'reqwest';
  // import when from 'when';
  
  exports.default = new AuthService();

/***/ },
/* 149 */
/***/ function(module, exports) {

  module.exports = require("bluebird");

/***/ },
/* 150 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _express = __webpack_require__(5);
  
  var _passport = __webpack_require__(142);
  
  var _passport2 = _interopRequireDefault(_passport);
  
  var _account = __webpack_require__(144);
  
  var _account2 = _interopRequireDefault(_account);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var router = new _express.Router();
  
  router.post('/register', function (req, res) {
    _account2.default.register(new _account2.default({ username: req.body.username }), req.body.password, function (err, account) {
      if (err) {
        console.log(err.name + ': ', err.message);
        if (err.name === 'UserExistsError') {
          res.status(400).send('User Exists');
        } else {
          res.status(500).send();
        }
      } else {
        _passport2.default.authenticate('local')(req, res, function () {
          console.log(req.user);
          res.status(200).send(req.user);
        });
      }
    });
  });
  
  router.post('/login', _passport2.default.authenticate('local'), function (req, res) {
    res.status(200).json({ user: req.user.username });
  });
  
  exports.default = router;

/***/ },
/* 151 */
/***/ function(module, exports, __webpack_require__) {

  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _express = __webpack_require__(5);
  
  var _bluebird = __webpack_require__(149);
  
  var _bluebird2 = _interopRequireDefault(_bluebird);
  
  var _frontMatter = __webpack_require__(152);
  
  var _frontMatter2 = _interopRequireDefault(_frontMatter);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var router = new _express.Router(); /**
                                       * React Starter Kit (https://www.reactstarterkit.com/)
                                       *
                                       * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
                                       *
                                       * This source code is licensed under the MIT license found in the
                                       * LICENSE.txt file in the root directory of this source tree.
                                       */
  
  router.post('/send', function (req, res1) {
    console.log(req.query);
  });
  
  exports.default = router;

/***/ },
/* 152 */
/***/ function(module, exports) {

  module.exports = require("front-matter");

/***/ }
/******/ ]);
//# sourceMappingURL=server.js.map